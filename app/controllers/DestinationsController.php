<?php

class DestinationsController extends \BaseController {

	/**
	 * Display a listing of destinations
	 *
	 * @return Response
	 */
	public function index()
	{
		$destinations = Destination::all();

		return View::make('destinations.index', compact('destinations'));
	}

	/**
	 * Show the form for creating a new destination
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('destinations.create');
	}

	/**
	 * Store a newly created destination in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Destination::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Destination::create($data);

                Session::flash('success_message', 'A new destination has been stored.');
                 
		return Redirect::route('destinations.index');
	}

	/**
	 * Display the specified destination.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$destination = Destination::findOrFail($id);

		return View::make('destinations.show', compact('destination'));
	}

	/**
	 * Show the form for editing the specified destination.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$destination = Destination::find($id);

		return View::make('destinations.edit', compact('destination'));
	}

	/**
	 * Update the specified destination in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$destination = Destination::findOrFail($id);
                $rules = Destination::$rules;
                $rules['code'] = 'required|unique:destinations,id,'.$id;
                
		$validator = Validator::make($data = Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$destination->update($data);

                Session::flash('success_message', 'The specified destination has been updated.');
                
		return Redirect::route('destinations.index');
	}

	/**
	 * Remove the specified destination from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Destination::destroy($id);

                Session::flash('success_message', 'The specified destination has been removed.');
                
		return Redirect::route('destinations.index');
	}

}
