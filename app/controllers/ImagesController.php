<?php

class ImagesController extends \BaseController {

    /**
     * Display a listing of images
     *
     * @return Response
     */
    public function index() {
        $images = Image::all();

        return View::make('images.index', compact('images'));
    }

    /**
     * Show the form for creating a new image
     *
     * @return Response
     */
    public function create() {
        return View::make('images.create')->with('destinations_id', Input::get('destinations_id'));
    }

    /**
     * Store a newly created image in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make($data = Input::all(), Image::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $filename = "destination_" . $data['destinations_id'] . "_" . time() . "." . $data['file']->getClientOriginalExtension();
        ;

        $img = ImageManipulation::make($file = Input::file('file'))->save('uploads/images/original/' . $filename);
        $img->backup();

        $img->fit(770, 500)->save('uploads/images/fullsize/' . $filename);

        $img->reset();
        $img->fit(440, 295)->save('uploads/images/bigsize/' . $filename);
        
        $img->reset();
        $img->fit(360, 240)->save('uploads/images/mediumsize/' . $filename);

        $img->reset();
        $img->fit(150)->save('uploads/images/thumbnails/' . $filename);

        $data = ['destinations_id' => $data['destinations_id'], 'file' => $filename];

        Image::create($data);

        Session::flash('success_message', 'A new Image has been stored.');

        return Redirect::route('images.relations', [$data['destinations_id']]);
    }

    /**
     * Display the specified image.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $image = Image::findOrFail($id);

        return View::make('images.show', compact('image'));
    }

    /**
     * Show the form for editing the specified image.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $image = Image::find($id);

        return View::make('images.edit', compact('image'));
    }

    /**
     * Update the specified image in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $image = Image::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Image::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $image->update($data);

        return Redirect::route('images.index');
    }

    /**
     * Remove the specified image from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $image = Image::find($id);

        $files = array(
            "uploads/images/original/" . $image->file,
            "uploads/images/fullsize/" . $image->file,
            "uploads/images/mediumsize/" . $image->file,
            "uploads/images/thumbnails/" . $image->file
        );
        File::delete($files);

        Image::destroy($id);

        Session::flash('success_message', 'The specified image has been removed.');

        return Redirect::route('images.relations', array($image->destinations_id));
    }

    public function relations($id) {
        $destination = Destination::findOrFail($id);

        $images = Image::where('destinations_id', $id)->get();

        return View::make('images.index', compact('images'))->with('destination', $destination);
    }

}
