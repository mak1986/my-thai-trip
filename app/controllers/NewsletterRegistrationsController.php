<?php

class NewsletterRegistrationsController extends \BaseController {

	/**
	 * Display a listing of newsletterregistrations
	 *
	 * @return Response
	 */
	public function index()
	{
		$newsletterregistrations = Newsletterregistration::all();

		return View::make('newsletterregistrations.index', compact('newsletterregistrations'));
	}

	/**
	 * Show the form for creating a new newsletterregistration
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('newsletterregistrations.create');
	}

	/**
	 * Store a newly created newsletterregistration in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Newsletterregistration::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Newsletterregistration::create($data);

		return Redirect::route('newsletter_registrations.thank_you');
	}

	/**
	 * Display the specified newsletterregistration.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$newsletterregistration = Newsletterregistration::findOrFail($id);

		return View::make('newsletterregistrations.show', compact('newsletterregistration'));
	}

	/**
	 * Show the form for editing the specified newsletterregistration.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$newsletterregistration = Newsletterregistration::find($id);

		return View::make('newsletterregistrations.edit', compact('newsletterregistration'));
	}

	/**
	 * Update the specified newsletterregistration in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$newsletterregistration = Newsletterregistration::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Newsletterregistration::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$newsletterregistration->update($data);

		return Redirect::route('newsletterregistrations.index');
	}

	/**
	 * Remove the specified newsletterregistration from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Newsletterregistration::destroy($id);

		return Redirect::route('newsletterregistrations.index');
	}

}
