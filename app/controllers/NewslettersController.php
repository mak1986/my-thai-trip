<?php

class NewslettersController extends \BaseController {

    private $emails;
    private $data;
    /**
     * Display a listing of newsletters
     *
     * @return Response
     */
    public function index() {
        $newsletters = Newsletter::all();

        return View::make('newsletters.index', compact('newsletters'));
    }

    /**
     * Show the form for creating a new newsletter
     *
     * @return Response
     */
    public function create() {
        return View::make('newsletters.create');
    }

    /**
     * Store a newly created newsletter in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make($data = Input::all(), Newsletter::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Newsletter::create($data);

        Session::flash('success_message', 'A new newsletter has been stored.');

        return Redirect::route('newsletters.index');
    }

    /**
     * Display the specified newsletter.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $newsletter = Newsletter::findOrFail($id);

        return View::make('newsletters.show', compact('newsletter'));
    }

    /**
     * Show the form for editing the specified newsletter.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $newsletter = Newsletter::find($id);

        return View::make('newsletters.edit', compact('newsletter'));
    }

    /**
     * Update the specified newsletter in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $newsletter = Newsletter::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Newsletter::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $newsletter->update($data);

        Session::flash('success_message', 'The specified newsletter has been updated.');

        return Redirect::route('newsletters.index');
    }

    /**
     * Remove the specified newsletter from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Newsletter::destroy($id);

        Session::flash('success_message', 'The specified newsletter has been removed.');

        return Redirect::route('newsletters.index');
    }

    public function send($id) {
        
        $newsletter = Newsletter::findOrFail($id);
        $this->data = ['topic'=>$newsletter->topic,'content'=>$newsletter->content];
        $this->emails = NewsletterRegistration::all();
        Mail::send('newsletters.template', $this->data, function($message) {
            foreach($this->emails as $email){
                $message->to($email->email)->subject('My Thai Trip - '.$this->data['topic']);
            }
        });
        Session::flash('success_message','The newsletter has been sent.');
        return Redirect::route('newsletters.index');
    }
    public function cancel($encodedEmail){        
        $email = NewsletterRegistration::where('email',base64_decode($encodedEmail))->first();
        $email->delete();
        return Redirect::route('newsletters.cancel.complete');
    }

}
