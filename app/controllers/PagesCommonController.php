<?php

class PagesCommonController extends \BaseController {

    protected $layout = 'layouts.page';

    public function home() {
        $content = Content::findOrFail(1);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    public function about_us() {
        $content = Content::findOrFail(2);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    public function contact_us() {
        $content = Content::findOrFail(3);
        return View::make('pages.contact', compact('content'))->with('cart',Cart::getCart());;
    }

    public function submitContact() {
        $data = Input::all();
        Mail::send('emails.contact', $data, function($message) {
            $message->to('kaset.tourism@gmail.com')->subject('Contact');            
        });
        return View::make('pages.contact_success')->with('cart',Cart::getCart());;
    }

    public function newsletterRegistrationThankYou() {
        return View::make('pages.newsletter_registrations_thank_you')->with('cart',Cart::getCart());;
    }
    public function newsletterCancelComplete(){
        return View::make('pages.newsletter_cancel_complete')->with('cart',Cart::getCart());
    }
    public function faq() {
        $content = Content::findOrFail(4);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    public function why_choose_us() {
        $content = Content::findOrFail(5);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    public function privacy_policy() {
        $content = Content::findOrFail(6);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    public function terms() {
        $content = Content::findOrFail(7);
        return View::make('pages.content', compact('content'))->with('cart',Cart::getCart());;
    }

    /**
     * Show the form for creating a new resource.
     * GET /pages/create
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /pages
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     * GET /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /pages/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
