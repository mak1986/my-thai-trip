<?php

class PagesCustomizeTourController extends \BaseController {

    protected $layout = 'layouts.page';

    public function customize_tour() {
         $this->layout = 'layouts.product';
        //Optimize later (bad query for promotion)
        $ordered_promotions = Promotion::whereRaw('from_date < NOW() and to_date > NOW()')->orderBy('created_at', 'DESC')->get();
        $promotions = [];
        foreach ($ordered_promotions as $promotion) {
            if (!array_key_exists($promotion->destinations_id, $promotions)) {
                $promotions[$promotion->destinations_id] = $promotion;
            }
        }
        $destinations = $this->getDestinationsWithNoDiscount($promotions);
        $content = Content::findOrFail(8);
        return View::make('pages.customize_tour', compact('content'))
                        ->with('destinations', $destinations)
                        ->with('promotions', $promotions)
                        ->with('cart', $this->getCart());
    }

    private function getDestinationsWithNoDiscount($promotions) {
        $destinations = array();
        foreach (Destination::all() as $destination) {
            if(!array_key_exists($destination->id, $promotions)){
                array_push($destinations, $destination);
            }
        }
        return $destinations;
    }

    public function show_product($id) {
        $this->layout = 'layouts.product';
        $destination = Destination::findOrFail($id);
        $reviews = Review::with('payments')->whereRaw('destinations_id='.$id.' and status=1')->get();
        $promotion = Promotion::whereRaw('from_date < NOW() and to_date > NOW() and destinations_id=' . $id)->orderBy('created_at', 'DESC')->first();
        return View::make('pages.product', compact('destination'))->with('promotion', $promotion)->with('reviews',$reviews)->with('cart', $this->getCart());
    }

    private function getCart() {
        $cart = Cart::getCart();
        if (!$cart->isEmpty()) {
            $order_id = $cart->getOrderId();
            $order = Order::findOrFail($order_id);
            if ($order->paid) {
                $cart->reset();
                $cart = Cart::getCart();
            }
        }
        return $cart;
    }

}
