<?php

class PaymentsController extends \BaseController {

    /**
     * Display a listing of payments
     *
     * @return Response
     */
    public function index() {
        $payments = Payment::all();

        return View::make('payments.index', compact('payments'));
    }

    /**
     * Show the form for creating a new payment
     *
     * @return Response
     */
    public function create() {
        return View::make('payments.create')->with('cart', Cart::getCart());
    }

    /**
     * Store a newly created payment in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make($data = Input::all(), Payment::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $payment = Payment::create($data);
        $order = Order::findOrFail($data['orders_id']);
        $order->paid = 1;
        $order->save();
        return Redirect::route('payments.show', array($payment->id));
    }

    /**
     * Display the specified payment.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $payment = Payment::findOrFail($id);

        return View::make('payments.show', compact('payment'))->with('cart', Cart::getCart());
    }

    /**
     * Show the form for editing the specified payment.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $payment = Payment::find($id);

        return View::make('payments.edit', compact('payment'));
    }

    /**
     * Update the specified payment in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $payment = Payment::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Payment::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $payment->update($data);

        return Redirect::route('payments.index');
    }

    /**
     * Remove the specified payment from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Payment::destroy($id);

        return Redirect::route('payments.index');
    }

    public function qaptcha() {
        $aResponse['error'] = false;

        if (isset($_POST['action']) && isset($_POST['qaptcha_key'])) {
            $_SESSION['qaptcha_key'] = false;

            if (htmlentities($_POST['action'], ENT_QUOTES, 'UTF-8') == 'qaptcha') {
                $_SESSION['qaptcha_key'] = $_POST['qaptcha_key'];
                echo json_encode($aResponse);
            } else {
                $aResponse['error'] = true;
                echo json_encode($aResponse);
            }
        } else {
            $aResponse['error'] = true;
            echo json_encode($aResponse);
        }
    }

}
