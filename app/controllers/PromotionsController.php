<?php

class PromotionsController extends \BaseController {

	/**
	 * Display a listing of promotions
	 *
	 * @return Response
	 */
	public function index()
	{                            
		$promotions = Promotion::all();

		return View::make('promotions.index', compact('promotions'));
	}

	/**
	 * Show the form for creating a new promotion
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('promotions.create')->with('destinations_id',Input::get('destinations_id'));
	}

	/**
	 * Store a newly created promotion in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Promotion::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Promotion::create($data);
                
                Session::flash('success_message', 'A new promotion has been stored.');

		return Redirect::route('promotions.relations', [$data['destinations_id']]);
	}

	/**
	 * Display the specified promotion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$promotion = Promotion::findOrFail($id);

		return View::make('promotions.show', compact('promotion'));
	}

	/**
	 * Show the form for editing the specified promotion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promotion = Promotion::find($id);

		return View::make('promotions.edit', compact('promotion'));
	}

	/**
	 * Update the specified promotion in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$promotion = Promotion::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Promotion::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$promotion->update($data);

		return Redirect::route('promotions.index');
	}

	/**
	 * Remove the specified promotion from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Promotion::destroy($id);

		return Redirect::route('promotions.index');
	}
        
        public function relations($id)
	{
                $destination = Destination::findOrFail($id);
		
                $promotions = Promotion::orderBy('created_at','DESC')->where('destinations_id',$id)->get();

		return View::make('promotions.index', compact('promotions'))->with('destination',$destination);;
	}

}
