<?php

class ReviewsController extends \BaseController {

	/**
	 * Display a listing of reviews
	 *
	 * @return Response
	 */
	public function index()
	{
		$reviews = Review::orderBy('created_at','DESC')->get();

		return View::make('reviews.index', compact('reviews'));
	}

	/**
	 * Show the form for creating a new review
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reviews.create');
	}

	/**
	 * Store a newly created review in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Review::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Review::create($data);
                Session::flash('success_message', 'Thank you! Your review has now been submitted.');
                return Redirect::route('customize_tour.show.product', [$data['destinations_id']]);
	}

	/**
	 * Display the specified review.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$review = Review::findOrFail($id);

		return View::make('reviews.show', compact('review'));
	}

	/**
	 * Show the form for editing the specified review.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = Review::find($id);

		return View::make('reviews.edit', compact('review'));
	}

	/**
	 * Update the specified review in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$review = Review::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Review::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$review->update($data);

		return Redirect::route('reviews.index');
	}
        public function setStatus($id,$status){
            $review = Review::findOrFail($id);
            $review->status = $status;
            $review->save();
            Session::flash('success_message', 'The review has been updated.');
            return Redirect::route('reviews.index');
        }
	/**
	 * Remove the specified review from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Review::destroy($id);
                Session::flash('success_message', 'The review has been deleted.');
		return Redirect::route('reviews.index');
	}
	public function delete($id)
	{
		Review::destroy($id);
                Session::flash('success_message', 'The review has been deleted.');
		return Redirect::route('reviews.index');
	}
}
