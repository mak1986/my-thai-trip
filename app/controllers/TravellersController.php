<?php

class TravellersController extends \BaseController {

    /**
     * Display a listing of travellers
     *
     * @return Response
     */
    public function index() {
        $orders = DB::table('orders')->where('paid', 1)->orderBy('tour_date', 'DESC')->get();
        $records = [];
        foreach ($orders as $order) {
            $traveller = Traveller::selectRaw('count(*) AS amounts')->where('orders_id', $order->id)->first();

            if (!array_key_exists($order->tour_date, $records)) {
                $records[$order->tour_date]['date'] = $order->tour_date;
                $records[$order->tour_date]['orders'] = 1;
                $records[$order->tour_date]['travellers'] = $traveller->amounts;
                $date = new DateTime($order->tour_date);
                $formatted_date = $date->format('d M Y');
                $records[$order->tour_date]['formatted_date'] = $formatted_date;
            } else {
                $records[$order->tour_date]['orders'] += 1;
                $records[$order->tour_date]['travellers'] += $traveller->amounts;
            }
        }
        return View::make('travellers.index', compact('records'));
    }

    /**
     * Show the form for creating a new traveller
     *
     * @return Response
     */
    public function create() {
        return View::make('travellers.create');
    }

    /**
     * Store a newly created traveller in storage.
     *
     * @return Response
     */
    public function store() {

        $cart = $this->initializeCart();

        //Get the tourists that has already been added to the cart.
        $touristsInCart = $this->getTouristInCartFromInput();

        if ($touristsInCart) {

            //Delete outersect tourists from cart and delete them from database.
            $deletedIds = $cart->deleteOutersectTourists($touristsInCart);
            if (count($deletedIds) > 0) {
                Traveller::destroy($deletedIds);
            }

            //Update tourists in cart. ex. change name,age
            $cart->updateTouristsInfo($touristsInCart);
        }

        //Add new tourists in cart.
        $newTourists = $this->makeNewTouristsFromInput($cart->getOrderId());
        $cart->addTourists($newTourists);
//        echo "<pre>";var_dump(Cart::getCart());die();
        //Get the existing item from cart or add the item into the cart and then return it.
        $item = $this->getItem($cart);

        //get tourists that are going to the destination.
        $joiningTourists = $this->getJoiningTourists($cart);

        //add item to each tourist and add tourists to the item. Add new records in traveller_destinations
        $cart->refresh($item, $joiningTourists);

        //update travel date
        $this->updateOrder($cart);




        return Redirect::route('customize_tour');
    }

    //set travel date and set order id
    private function initializeCart() {
        $cart = Cart::getCart();
        $cart->setTravelDate(Input::get('travel_date'));
        if ($cart->getOrderId() == null) {
            $order = Order::create(array('tour_date' => $cart->getTravelDate(), 'paid' => 0));
            $cart->setOrderId($order->id);
        }
        return $cart;
    }

    private function getItem($cart) {
        $item = $this->getItemFromInput();
        if (!$cart->hasItem($item->getId())) {
            $cart->addItem($item);
        }
        return $cart->getItemById($item->getId());
    }

    private function getItemFromInput() {
        $destination = Destination::findOrFail(Input::get('destination_id'));
        $promotion = Promotion::whereRaw('from_date < NOW() and to_date > NOW() and destinations_id=' . Input::get('destination_id'))->orderBy('created_at', 'DESC')->first();

        if ($promotion) {
            $item = new Item($destination->id, $destination->code, $destination->name, $destination->price, $promotion->discount);
        } else {
            $item = new Item($destination->id, $destination->code, $destination->name, $destination->price, 0);
        }
        return $item;
    }

    private function getJoiningTourists($cart) {
        $checks = Input::get('check');
        $joiningTourists = array();
        if ($checks) {
            foreach ($checks as $key => $value) {
                $data = explode("|", $value);
                $id = $data[1];
                array_push($joiningTourists, $cart->getTouristByName(Input::get('first_name')[$id], Input::get('last_name')[$id]));
            }
        }
        return $joiningTourists;
    }

    private function makeNewTouristsFromInput($orderId) {
        $tourists = array();
        foreach (Input::get('id') as $value) {
            $data = explode("|", $value);
            if ($data[0] == 'new') {
                $id = $data[1];
                $traveller = Traveller::create(array(
                            'orders_id' => $orderId,
                            'first_name' => Input::get('first_name')[$id],
                            'last_name' => Input::get('last_name')[$id],
                            'age' => Input::get('age')[$id]
                                )
                );
                $tourist = new Tourist($traveller->id, $traveller->first_name, $traveller->last_name, $traveller->age);
                array_push($tourists, $tourist);
                ;
            }
        }
        return $tourists;
    }

    private function getTouristInCartFromInput() {
        $tourists = array();
        foreach (Input::get('id') as $value) {
            $data = explode("|", $value);
            if ($data[0] == 'cart') {
                $id = $data[1];
                $tourist = new Tourist($id, Input::get('first_name')[$id], Input::get('last_name')[$id], Input::get('age')[$id]);
                array_push($tourists, $tourist);
            }
        }
        return $tourists;
    }

    private function updateOrder($cart) {
        $order = Order::findOrFail($cart->getOrderId());
        $order->tour_date = $cart->getTravelDate();
        $order->save();
    }

    /**
     * Display the specified traveller.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($date) {
        $orders = Order::with('payment')->whereRaw("tour_date='{$date}' AND paid='1'")->get();
        $date = new DateTime($date);
        $formatted_date = $date->format('d M Y');
        return View::make('travellers.show', compact('orders'))->with('date', $formatted_date);
    }

    /**
     * Show the form for editing the specified traveller.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $traveller = Traveller::find($id);

        return View::make('travellers.edit', compact('traveller'));
    }

    /**
     * Update the specified traveller in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $traveller = Traveller::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Traveller::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $traveller->update($data);

        return Redirect::route('travellers.index');
    }

    public function delete($id) {
        $cart = Cart::getCart();
        $cart->deleteItem($id);
        return Redirect::back();
    }

    /**
     * Remove the specified traveller from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Traveller::destroy($id);

        return Redirect::route('travellers.index');
    }

}
