<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDestinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destinations', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('code',9);
			$table->string('name', 128); 
                        $table->string('address', 255);
                        $table->integer('telephone');
                        $table->text('overview');
                        $table->text('important_info');
                        $table->string('duration',64);
                        $table->double('price', 5, 2);
                        $table->timestamps();
                        $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destinations');
	}

}
