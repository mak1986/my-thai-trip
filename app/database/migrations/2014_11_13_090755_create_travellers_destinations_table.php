<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravellersDestinationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('travellers_destinations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('destinations_id')->unsigned();
            $table->foreign('destinations_id')->references('id')->on('destinations')->onDelete('cascade');
            $table->integer('travellers_id')->unsigned();
            $table->foreign('travellers_id')->references('id')->on('travellers')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('travellers_destinations');
    }

}
