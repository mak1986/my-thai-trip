<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('payments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('orders_id')->unsigned();
            $table->foreign('orders_id')->references('id')->on('orders')->onDelete('cascade');
            $table->string('name',128);
            $table->string('billing_address_line_1',64);
            $table->string('billing_address_line_2',64);
            $table->string('city',128);
            $table->string('country',64);
            $table->integer('postal_code');
            $table->bigInteger('credit_card_number');
            $table->smallInteger('card_security_number');
            $table->smallInteger('exp_month');
            $table->smallInteger('exp_year');
            $table->smallInteger('area_code');
            $table->integer('phone_number');
            $table->string('email',100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('payments');
    }

}
