<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 1) as $index)
		{
			User::create([
                            'email'=>'mak.jacobsen@gmail.com',
                            'password'=>'$2y$10$EUigMNYC0Ynql1L/H13iIOUNFHxY3drC7BsrLXz.V8qePgMa71OfO'
			]);
		}
	}

}