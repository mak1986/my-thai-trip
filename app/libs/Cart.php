<?php

class Cart {

    private $tourists = array();
    private $items = array();
    private $orderId = null;
    private $travelDate = null;

    private function __construct() {
        
    }

    public static function getCart() {
        if (!Session::get('cart')) {
            Session::set('cart', new Cart());
        }
        return Session::get('cart');
    }

    public function getTourists() {
        return $this->tourists;
    }

    public function getItems() {
        return $this->items;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function getTravelDate() {
        return $this->travelDate;
    }

    public function getFormatedTravelDate() {
        $date = new DateTime($this->travelDate);
        return $date->format('d M Y');
    }

    public function travelDateIsSet() {
        if ($this->travelDate == null) {
            return false;
        }
        return true;
    }

    public function setTravelDate($travelDate) {
        $this->travelDate = $travelDate;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function addtourist(Tourist $tourist) {
        array_push($this->tourists, $tourist);
    }

    public function addtourists($tourists) {
        foreach ($tourists as $tourist) {
            $this->addtourist($tourist);
        }
    }

    public function addItem(Item $item) {
        array_push($this->items, $item);
    }

    public function getTourist(Tourist $needle) {

        foreach ($this->tourists as $tourist) {
            //$tourist = unserialize(serialize($tourist));
            if ($tourist->same($needle)) {
                return $tourist;
            }
        }
        return false;
    }

    public function getTouristByName($firstName, $lastName) {
        foreach ($this->tourists as $tourist) {
            if ($tourist->getFirstName() == $firstName && $tourist->getLastName() == $lastName) {
                return $tourist;
            }
        }
        return false;
    }

    public function getTotalPrice() {
        $total = 0;
        foreach ($this->tourists as $tourist) {
            $total += $tourist->getTotalPrice();
        }
        return number_format($total, 2, '.', '');
    }

    public function hasItem($id) {
        foreach ($this->items as $item) {
            //$item = unserialize(serialize($item));
            if ($item->getId() == $id) {
                return true;
            }
        }
        return false;
    }

    public function getItemById($id) {
        foreach ($this->items as $item) {
            //$item = unserialize(serialize($item));
            if ($item->getId() == $id) {
                return $item;
            }
        }
        return false;
    }

    public function isEmpty() {
        if (count($this->items) == 0 && count($this->tourists) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public function refresh(Item $item, $joiningTourists) {

        foreach ($this->tourists as $tourist) {
            if($this->touristExists($tourist, $joiningTourists)) {
                if (!$item->hasTourist($tourist)) {
                    $item->addTourist($tourist);
                }
                if (!$tourist->hasItem($item)) {

                    $tourist->addItem($item);

                    Traveller_destination::create(array(
                        'destinations_id' => $item->getId(),
                        'travellers_id' => $tourist->getId()
                            )
                    );
                }
            } else {
                if ($item->hasTourist($tourist)) {
                    $item->deleteTourist($tourist);
                }
                if ($tourist->hasItem($item)) {

                    $tourist->deleteItem($item);

                    Traveller_destination::whereRaw("destinations_id = {$item->getId()} AND travellers_id = {$tourist->getId()}")
                            ->delete();
                }
            }
        }
        foreach ($this->items as $key => $item) {
            if (!$item->hasTourists()) {
                unset($this->items[$key]);
            }
        }
        if(count($this->items)==0){
            $this->reset();
        }
    }

//    public function refresh(Item $item) {
//
//        foreach ($this->tourists as $tourist) {
//
//            if (!$item->hasTourist($tourist)) {
//                $item->addTourist($tourist);
//            }
//            if (!$tourist->hasItem($item)) {
//
//                $tourist->addItem($item);
//
//                Traveller_destination::create(array(
//                    'destinations_id' => $item->getId(),
//                    'travellers_id' => $tourist->getId()
//                        )
//                );
//            }
//        }
//        foreach ($this->items as $key => $item) {
//            if (!$item->hasTourists()) {
//                unset($this->items[$key]);
//            }
//        }
//    }
    public function updateTouristsInfo($setA) {
        foreach ($setA as $element) {
            $tourist = $this->getTourist($element);
            if ($tourist) {
                $tourist->setFirstName($element->getFirstName());
                $tourist->setLastName($element->getLastName());
                $tourist->setAge($element->getAge());
                $traveller = Traveller::findOrFail($tourist->getId());
                $traveller->first_name = $tourist->getFirstName();
                $traveller->last_name = $tourist->getLastName();
                $traveller->age = $tourist->getAge();
                $traveller->save();
            }
        }
    }
    public function deleteItem($id){
        $item = $this->getItemById($id);
        foreach ($this->tourists as $tourist) {

                if ($item->hasTourist($tourist)) {
                    $item->deleteTourist($tourist);
                }
                if ($tourist->hasItem($item)) {

                    $tourist->deleteItem($item);

                    Traveller_destination::whereRaw("destinations_id = {$item->getId()} AND travellers_id = {$tourist->getId()}")
                            ->delete();
                }

        }
        foreach ($this->items as $key => $itemIterator) {
            if ($itemIterator->same($item)) {
                unset($this->items[$key]);
            }
        }
        if(count($this->items)==0){
            $this->reset();
        }
    }
    public function deleteOutersectTourists($setA) {
        $deletedIds = array();
        foreach ($this->tourists as $tourist) {
            if (!$this->touristExists($tourist, $setA)) {
                $this->deleteTourist($tourist);
                array_push($deletedIds, $tourist->getId());
            }
        }
        return $deletedIds;
    }

    private function touristExists($needle, $haystack) {
        //$needle = unserialize(serialize($needle));
        foreach ($haystack as $element) {
            if ($needle->same($element)) {
                return true;
            }
        }
        return false;
    }

    private function deleteTourist(Tourist $needle) {
        //$needle = unserialize(serialize($needle));
        foreach ($this->tourists as $key => $tourist) {
            if ($tourist->same($needle)) {
                unset($this->tourists[$key]);
            }
        }
        foreach ($this->items as $item) {
            if ($item->hasTourist($needle)) {
                //$item = unserialize(serialize($item));
                $item->deleteTourist($needle);
            }
        }
    }

    public function calculateVat($value) {
        return number_format(($value * 7) / 100, 2, '.', '');
    }

    public function reset() {
        Session::forget('cart');
        Session::set('cart', new Cart());
    }

}
