<?php

class Item {

    private $tourists = array();
    private $id;
    private $code;
    private $name;
    private $originalPrice;
    private $price;
    private $discount;

    public function __construct($id, $code, $name, $originalPrice, $discount) {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->originalPrice = number_format($originalPrice, 2, '.', '');
        $this->discount = number_format($discount, 2, '.', '');
        if ($discount > 0) {
            $this->price = number_format($this->originalPrice - ($this->originalPrice * $this->discount) / 100, 2, '.', '');
            ;
        } else {
            $this->price = $this->originalPrice;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function getOriginalPrice() {
        return number_format($this->originalPrice, 2,'.','');
    }

    public function getPrice() {
        return number_format($this->price, 2,'.','');
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function getTourists() {
        return $this->tourists;
    }

    public function addTourist(Tourist $tourist) {
        array_push($this->tourists, $tourist);
    }

    public function countTourists() {
        return count($this->tourists);
    }
    public function countAdults(){
        $count = 0;
        foreach($this->tourists as $tourist){
            if($tourist->isAdult()){
                $count++;
            }
        }
        return $count;
    }
    public function countChildren(){
        $count = 0;
        foreach($this->tourists as $tourist){
            if($tourist->isChild()){
                $count++;
            }
        }
        return $count;
    }
    public function getChildPrice(){
        return number_format($this->getPrice()/2.00,2,'.','');
    }
    public function getTotalPrice() {
        return number_format($this->getTotalPriceAdult()+$this->getTotalPriceChild(), 2,'.','');;
    }
    public function getTotalPriceChild(){
        $total = 0;
         foreach ($this->tourists as $tourist) {
            if ($tourist->isChild()) {
                $total += $this->getPrice()/2;
            }
        }
        return number_format($total, 2,'.','');;
    }
    public function getTotalPriceAdult(){
        $total = 0;
         foreach ($this->tourists as $tourist) {
            if ($tourist->isAdult()) {
                $total += $this->getPrice();
            }
        }
        return number_format($total, 2,'.','');;
    }
    public function deleteTourist(Tourist $needle) {
        foreach ($this->tourists as $key => $tourist) {
            if ($tourist->same($needle)) {
                unset($this->tourists[$key]);
                return;
            }
        }
    }

    public function hasTourist(Tourist $needle) {
        foreach ($this->tourists as $tourist) {
            if ($tourist->same($needle)) {
                return true;
            }
        }
        return false;
    }
    public function hasTourists(){
        if($this->countTourists()>0){
            return true;
        }
        return false;
    }
    public function same($needle) {
        if ($this->getId() == $needle->getId()) {
            return true;
        }
        return false;
    }

}
