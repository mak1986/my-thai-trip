<?php

class Tourist {

    private $id;
    private $firstName;
    private $lastName;
    private $age;
    private $items = array();

    public function __construct($id, $firstName, $lastName, $age) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
    }

    public function getId() {
        return $this->id;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getAge() {
        return $this->age;
    }

    public function getItems() {
        return $this->items;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setAge($age) {
        $this->age = $age;
    }

    public function isChild() {
        if ($this->age < 12) {
            return true;
        } else {
            return false;
        }
    }

    public function isAdult() {
        return !$this->isChild();
    }

    public function getFullName() {
        return $this->firstName . " " . $this->lastName;
    }

    public function same(Tourist $tourist) {
        if ($this->id == $tourist->getId()) {
            return true;
        } else {
            return false;
        }
    }

    public function addItem(Item $item) {
        array_push($this->items, $item);
    }

    public function hasItem(Item $needle) {
        foreach ($this->items as $item) {
            if ($item->same($needle)) {
                return true;
            }
        }
        return false;
    }

    public function hasItemId($id) {
        foreach ($this->items as $item) {
            if ($item->getId() == $id) {
                return true;
            }
        }
        return false;
    }

    public function removeItemById($id) {
        foreach ($this->items as $key => $item) {
            if ($item->getId() == $id) {
                unset($this->items[$key]);
            }
        }
    }

    public function getTotalPrice() {
        $total = 0;
        foreach ($this->items as $item) {
            if ($this->isChild()) {
                $total += $item->getPrice() / 2.00;
            } else {
                $total += $item->getPrice();
            }
        }
        return number_format($total, 2, '.', '');
    }

    public function deleteItem(Item $needle) {
        foreach ($this->items as $key => $item) {
            if ($item->same($needle)) {
                unset($this->items[$key]);
                return;
            }
        }
    }

}
