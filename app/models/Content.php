<?php

class Content extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
        'topic' => 'required|between:3,255',
        'text' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['topic', 'text'];
    // Soft delete

}
