<?php

class Destination extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
        'code' => 'required|unique:destinations',
        'name' => 'required|between:4,255',
        'address' => 'required|between:4,255',
        'telephone' => '',
        'overview' => 'required',
        'important_info' => 'required',
        'duration' => 'required|between:4,255',
        'price' => 'required|numeric|between:10,99999'
    ];
    // Don't forget to fill this array
    protected $fillable = ['code', 'name', 'address', 'telephone', 'overview', 'important_info', 'duration', 'price'];

    public function images() {
        return $this->hasMany('Image', 'destinations_id');
    }
    public function reviews() {
        return $this->hasMany('Review','destinations_id');
    }
}
