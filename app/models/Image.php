<?php

class Image extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
        'destinations_id'=>'required|min:1|numeric',
        'file' => 'required|mimes:jpeg,bmp,png'
    ];
    // Don't forget to fill this array
    protected $fillable = ['destinations_id', 'file'];

}
