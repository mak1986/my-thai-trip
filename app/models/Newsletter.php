<?php

class Newsletter extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'topic' => 'required|between:4,255',
                'content'=>'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['topic','content'];

}