<?php

class NewsletterRegistration extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'email' => 'required|email|unique:newsletter_registrations,email'
	];

	// Don't forget to fill this array
	protected $fillable = ['email'];

}