<?php

class Order extends \Eloquent {

	// Don't forget to fill this array
	protected $fillable = ['paid','tour_date'];
        public function travellers(){
            return $this->hasMany('Traveller','orders_id');
        }
        public function payment(){
            return $this->hasOne('Payment','orders_id');
        }
}