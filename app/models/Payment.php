<?php

class Payment extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name'=>'required',
                'billing_address_line_1'=>'required',
                'billing_address_line_2'=>'',
                'city'=>'required',
                'country'=>'required',
                'postal_code'=>'required|numeric',
                'credit_card_number'=>'required|digits_between:16,16',
                'card_security_number'=>'required|digits_between:3,3',
                'exp_month'=>'required|digits_between:1,2',
                'exp_year'=>'required|digits_between:2,2',
                'area_code'=>'required|numeric',
                'phone_number'=>'required|numeric'
	];

	// Don't forget to fill this array
	protected $fillable = ['orders_id','name','billing_address_line_1','billing_address_line_2',
            'city','country','postal_code','credit_card_number','card_security_number','exp_month',
            'exp_year','area_code','phone_number','email'];

}