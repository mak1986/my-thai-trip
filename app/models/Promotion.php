<?php

class Promotion extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
            'discount' => 'required|between:1,50',
            'from_date'=> 'required|date|before:to_date',
            'to_date'=>'required|date|different:from_date'
    ];
    // Don't forget to fill this array
    protected $fillable = ['destinations_id','discount','from_date','to_date'];

    public function destination() {
        return $this->belongsTo('Destination','destinations_id');
    }

}
