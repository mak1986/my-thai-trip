<?php

class Review extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
        'review_email' => 'required|email|exists:payments,email',
        'review' => 'required',
        'destinations_id' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['review_email', 'destinations_id', 'review','status','created_at'];

    public function payments() {
        return $this->hasMany('Payment','email','review_email');
    }
    public function destination(){
        return $this->belongsTo('Destination','destinations_id');
    }
}
