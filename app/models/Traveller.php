<?php

class Traveller extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'first_name[]' => 'required',
                'last_name[]' => 'required',
                'age[]'=>'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['orders_id','first_name','last_name','age'];
        public function orders(){
            return $this->belongsTo('Order','orders_id');
        }
}