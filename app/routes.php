<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() {
    return Redirect::route('home');
});

Route::get('/home',array('as'=>'home','uses'=>'PagesCommonController@home'));
Route::get('/contact-us',array('as'=>'contact_us','uses'=>'PagesCommonController@contact_us'));
Route::get('/about-us',array('as'=>'about-us','uses'=>'PagesCommonController@about_us'));
Route::get('/terms',array('as'=>'terms','uses'=>'PagesCommonController@terms'));
Route::get('/privacy-policy',array('as'=>'privacy_policy','uses'=>'PagesCommonController@privacy_policy'));
Route::get('/faq',array('as'=>'faq','uses'=>'PagesCommonController@faq'));
Route::get('/why-choose-us',array('as'=>'why_choose_us','uses'=>'PagesCommonController@why_choose_us'));
Route::get('/customize-tour',array('as'=>'customize_tour','uses'=>'PagesCustomizeTourController@customize_tour'));

//Customize
Route::get('customize-tour/product/{destination_id}',array('as'=>'customize_tour.show.product','uses'=>'PagesCustomizeTourController@show_product'));
//Travellers
Route::resource('travellers', 'TravellersController');
Route::get('travellers/delete/{id}',array('as'=>'travellers.delete','uses'=>'TravellersController@delete'));
//NewsletterRegistration
Route::resource('newsletter_registrations', 'NewsletterRegistrationsController');
Route::get('newsletter-registrations/thank-you', array('as'=>'newsletter_registrations.thank_you','uses'=>'PagesCommonController@newsletterRegistrationThankYou'));
Route::get('newsletter/cancel-complete', array('as'=>'newsletters.cancel.complete','uses'=>'PagesCommonController@newsletterCancelComplete'));
Route::get('newsletters/cancel/{email}',array('as'=>'newsletters.cancel','uses'=>'NewslettersController@cancel'));
//Reviews
Route::resource('reviews', 'ReviewsController');
Route::get('reviews/{id}/delete',array('as'=>'reviews.delete','uses'=>'ReviewsController@delete'));
Route::get('reviews/setStatus/{id}/{status}',array('as'=>'reviews.setStatus','uses'=>'reviewsController@setStatus'));
//Contact
Route::post('/contact-us',array('as'=>'contact_us.submitContact','uses'=>'PagesCommonController@submitContact'));
//Payment
Route::resource('payments', 'PaymentsController');
Route::post('/payments/qaptcha',array('uses'=>'PaymentsController@qaptcha'));

Route::get('/login', function() {
    if(!Auth::check()){
        return View::make('login.login');
    }else{
        return Redirect::to('contents');
    }
});
Route::get('/logout', function() {
    Auth::logout();
    return Redirect::to('/home');
});

Route::post('/login', function() {
    $user=['email'=>Input::get('email'),'password'=>Input::get('password')];
    $validator = Validator::make($data = $user, User::$rules);
    if ($validator->passes()) {
        // Try to log the user in.
        if (Auth::attempt($user)) {
            return Redirect::intended('contents')->with('message', 'You have logged in successfully');
        }
        $errors =  new Illuminate\Support\MessageBag;
        $errors->add('password','Email and/or password invalid.');
        return Redirect::to('login')->withErrors($errors)->withInput(Input::except('password'));
    } else {
        // Something went wrong.
        return Redirect::back()->withErrors($validator)->withInput(Input::except('password'));
    }
});
Route::group(array('before' => 'auth'), function() {
    Route::resource('contents', 'ContentsController');
    Route::resource('destinations', 'DestinationsController');
    
    Route::get('newsletters/send/{id}', array('as' => 'newsletters.send', 'uses' => 'NewslettersController@send'));
    Route::resource('newsletters', 'NewslettersController');
    
    Route::resource('images', 'ImagesController');
    Route::get('/destinations/{destinations_id}/images',array('as'=>'images.relations','uses' => 'ImagesController@relations'));
    
    Route::resource('promotions', 'PromotionsController');
    Route::get('/destinations/{destinations_id}/promotions',array('as'=>'promotions.relations','uses' => 'PromotionsController@relations'));
});
