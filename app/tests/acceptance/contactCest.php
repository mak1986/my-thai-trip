<?php
use \AcceptanceTester;

class contactCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function tryToSubmitAContactForm(AcceptanceTester $I)
    {
        $I->amOnPage('/contact-us');
        $I->wantTo('submit a contact form');
        $I->seeElement('#input-my-name');
        $I->fillField('#input-my-name', 'John Doe');
        $I->seeElement('#input-my-email-address');
        $I->fillField('#input-my-email-address', 'John.Doe@mail.com');
        $I->seeElement('#input-my-phone-area-code');
        $I->fillField('#input-my-phone-area-code', '0066');
        $I->seeElement('#input-my-phone-number');
        $I->fillField('#input-my-phone-number', '12345678');
        $I->seeElement('#input-my-fax-area-code');
        $I->fillField('#input-my-fax-area-code', '0066');
        $I->seeElement('#input-my-fax-number');
        $I->fillField('#input-my-fax-number', '87654321');
        $I->seeElement('#input-product-name');
        $I->seeElement('#input-product-code');
        $I->seeElement('#input-addtional-comment');
        $I->fillField('#input-addtional-comment', 'test comment...');
        $I->seeElement('#input-submit');
        $I->click('#input-submit');
        $I->seeInCurrentUrl('/contact-us/thankyou');
    }
}