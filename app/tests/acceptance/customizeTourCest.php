<?php

use \AcceptanceTester;

class customizeTourCest {

    public function _before(AcceptanceTester $I) {
        
    }

    public function _after(AcceptanceTester $I) {
        
    }
    
    public function tryToBuyOneDestinationForOnePerson(AcceptanceTester $I) {
        $I->wantTo('buy a customize package with one destination for one person');
        $I->amOnPage('/customized-your-own-tour');
        $I->seeElement('#btn-start-customizing');
        $I->click('#btn-start-customizing');

        $I->seeInCurrentUrl('/customized-your-own-tour/add-travellers');
        $I->seeElement('#input-traveller-first-name-1');
        $I->fillField('#input-traveller-first-name-1', 'John');
        $I->seeElement('#input-traveller-last-name-1');
        $I->fillField('#input-traveller-last-name-1', 'Doe');
        $I->seeElement('#btn-continue');
        $I->click('#btn-continue');

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#destination-1');
        $I->click('#destination-1');

        $I->seeInCurrentUrl('/destination/1');
        $I->see('John Doe');
        $I->seeCheckboxIsChecked('#input-checkbox-traveller-1');
        $I->seeElement('#btn-add-to-cart');
        $I->click('#btn-add-to-cart');

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#btn-cart');
        $I->click('#btn-cart');
        
        $I->seeInCurrentUrl('/cart');
        $I->seeElement('#btn-checkout');
        $I->click('#btn-checkout');

        $I->seeInCurrentUrl('/checkout');
        $I->seeElement('#input-my-phone-area-code');
        $I->fillField('#input-my-phone-area-code', '0066');
        $I->seeElement('#input-my-phone-number');
        $I->fillField('#input-my-phone-number', '12345678');
        $I->seeElement('#input-my-email-address');
        $I->fillField('#input-my-email-address', 'John.Doe@mail.com');
        $I->seeElement('#input-verify-my-email-address');
        $I->fillField('#input-verify-my-email-address', 'John.Doe@mail.com');
        $I->seeElement('#input-first-name-on-credit-card');
        $I->seeElement('#input-last-name-on-credit-card');
        $I->seeElement('#input-billing-address-line-1');
        $I->seeElement('#input-billing-address-line-2');
        $I->seeElement('#input-city');
        $I->seeElement('#input-select-country');
        $I->seeElement('#input-postcode-zip');
        $I->seeElement('#input-select-credit-card-type');
        $I->seeElement('#input-credit-card-number');
        $I->seeElement('#input-card-security-number');
        $I->seeElement('#input-expiry-date-month');
        $I->seeElement('#input-expiry-date-year');
        $I->fillField('#input-first-name-on-credit-card', 'John');
        $I->fillField('#input-last-name-on-credit-card', 'Doe');
        $I->fillField('#input-billing-address-line-1', 'test address line 1');
        $I->fillField('#input-billing-address-line-2', 'test address line 2');
        $I->fillField('#input-city', 'test city');
        $I->selectOption('#input-select-country', 'Denmark');
        $I->fillField('#input-postcode-zip', '1000');
        $I->selectOption('#input-select-credit-card-type', 'Visa');
        $I->fillField('#input-credit-card-number', '1111 1111 1111 1111');
        $I->fillField('#input-card-security-number', '111');
        $I->fillField('#input-expiry-date-month', '01');
        $I->fillField('#input-expiry-date-year', '2050');
        $I->seeElement('#btn-book');
        $I->click('#btn-book');

        $I->seeInCurrentUrl('/customized-your-own-tour/thankyou');
        $I->seeElement('#btn-download-receipt');
    }
public function tryToBuyOneDestinationForOnePersonButIHaveAddedTwoTravellers(AcceptanceTester $I) {
        $I->wantTo('buy a customize package with one destination for one person but I have added two travellers');
        $I->amOnPage('/customized-your-own-tour');
        $I->seeElement('#btn-start-customizing');
        $I->click('#btn-start-customizing');

        $I->seeInCurrentUrl('/customized-your-own-tour/add-travellers');
        $I->seeElement('#input-traveller-first-name-1');
        $I->fillField('#input-traveller-first-name-1', 'John');
        $I->seeElement('#input-traveller-last-name-1');
        $I->fillField('#input-traveller-last-name-1', 'Doe');
        $I->dontSeeElement('#input-traveller-first-name-2');
        $I->dontSeeElement('#input-traveller-last-name-2');
        $I->seeElement('#btn-add-traveller');
        $I->click('#btn-add-traveller');
        $I->seeElement('#input-traveller-first-name-2');
        $I->fillField('#input-traveller-first-name-2', 'Tom');
        $I->seeElement('#input-traveller-last-name-2');
        $I->fillField('#input-traveller-last-name-2', 'Hanks');
        $I->seeElement('#btn-continue');
        $I->click('#btn-continue');

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#destination-1');
        $I->click('#destination-1');

        $I->seeInCurrentUrl('/destination/1');
        $I->see('John Doe');
        $I->seeCheckboxIsChecked('#input-checkbox-traveller-1');
        $I->see('Tom Hanks');
        $I->seeCheckboxIsChecked('#input-checkbox-traveller-2');
        $I->uncheckOption('#input-checkbox-traveller-2');
        $I->seeElement('#btn-add-to-cart');
        $I->click('#btn-add-to-cart');

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#btn-cart');
        $I->click('#btn-cart');
        
        $I->seeInCurrentUrl('/cart');
        $I->dontSeeElement('#btn-checkout');
        $I->seeElement('#notice-travellers-needs-destinations');
    }
    public function tryToRemoveOneDestinationFromAPackageWithTwoDestinations(AcceptanceTester $I) {
        $I->wantTo('remove a destination from a package with 2 desitinations');
        $I->amOnPage('/customized-your-own-tour');
        $I->seeElement('#btn-start-customizing');
        $I->click('#btn-start-customizing');

        $I->seeInCurrentUrl('/customized-your-own-tour/add-travellers');
        $I->seeElement('#input-traveller-first-name-1');
        $I->fillField('#input-traveller-first-name-1', 'John');
        $I->seeElement('#input-traveller-last-name-1');
        $I->fillField('#input-traveller-last-name-1', 'Doe');
        $I->seeElement('#btn-continue');
        $I->click('#btn-continue');

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#destination-1');
        $I->click('#destination-1');

        $I->seeInCurrentUrl('/destination/1');
        $I->see('John Doe');
        $I->seeCheckboxIsChecked('#input-checkbox-traveller-1');
        $I->seeElement('#btn-add-to-cart');
        $I->click('#btn-add-to-cart');
        
        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->seeElement('#destination-2');
        $I->click('#destination-2');

        $I->seeInCurrentUrl('/destination/2');
        $I->see('John Doe');
        $I->seeCheckboxIsChecked('#input-checkbox-traveller-1');
        $I->seeElement('#btn-add-to-cart');
        $I->click('#btn-add-to-cart');
        

        $I->seeInCurrentUrl('/customized-your-own-tour');
        $I->see('2', '#destinations-count');        
        $I->seeElement('#btn-cart');
        $I->click('#btn-cart');
                
        $I->seeInCurrentUrl('/cart');
        $I->seeElement('#destination-2 .btn-remove');
        $I->click('#destination-2 .btn-remove');
        
        $I->seeInCurrentUrl('/cart');
        $I->dontSeeElement('#destination-2');
        $I->see('1', '#destinations-count'); 
    }

}
