<?php

use \AcceptanceTester;

class guestGeneralCest {

    public function _before(AcceptanceTester $I) {
        $I->amOnPage('/');
    }

    public function _after(AcceptanceTester $I) {
        
    }

    public function tryToGoToFrontPage(AcceptanceTester $I) { 
        $I->amOnPage('/');
        $I->wantTo('go to the front page');
        $I->canSeeElement('#btn-home');
        $I->click('#btn-home');
        $I->seeInCurrentUrl('/home');
        $I->seeElement('#header-image');   
    }
    public function tryToGoToAboutUs(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the about us page');
        $I->canSeeElement('#btn-about-us');
        $I->click('#btn-about-us');
        $I->seeInCurrentUrl('/about-us');
    }
    public function tryToGoToContactUs(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the contact us page');
        $I->canSeeElement('#btn-contact-us');
        $I->click('#btn-contact-us');
        $I->seeInCurrentUrl('/contact-us');
    }
    public function tryToGoToFAQ(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the contact FAQ page');
        $I->canSeeElement('#btn-faq');
        $I->click('#btn-faq');
        $I->seeInCurrentUrl('/faq');
    }
    public function tryToGoToWhyChooseUs(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the contact why choose us page');
        $I->canSeeElement('#btn-why-choose-us');
        $I->click('#btn-why-choose-us');
        $I->seeInCurrentUrl('/why-choose-us');
    }
    public function tryToGoToPrivacyPolicy(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the contact privacy policy page');
        $I->canSeeElement('#btn-privacy-policy');
        $I->click('#btn-privacy-policy');
        $I->seeInCurrentUrl('/privacy-policy');
    }
    public function tryToGoToTerms(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the contact terms page');
        $I->canSeeElement('#btn-terms');
        $I->click('#btn-terms');
        $I->seeInCurrentUrl('/terms');
    }
    
    public function tryToGoToCustomizedYourOwnTour(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the customized your own tour page');
        $I->canSeeElement('#btn-customized-your-own-tour');
        $I->click('#btn-customized-your-own-tour');
        $I->seeInCurrentUrl('/customized-your-own-tour');
    }
    public function tryToGoToNewsletter(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->wantTo('go to the newsletter page');
        $I->canSeeElement('#btn-newsletter');
        $I->click('#btn-newsletter');
        $I->seeInCurrentUrl('/newsletter');
    }
}
