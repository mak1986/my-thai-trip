<?php
use \AcceptanceTester;

class newsletterCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function tryToSignUpForNewsletter(AcceptanceTester $I)
    {
        $I->wantTo('sign up for newsletter');
        $I->amOnPage('/newsletter');
        $I->seeElement('#input-email');
        $I->fillField('#input-email', 'John.Doe@mail.com');
        $I->seeElement('#input-submit');
        $I->click('#input-submit');
        $I->seeInCurrentUrl('/newsletter/thankyou');
    }
}