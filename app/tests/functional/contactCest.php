<?php
use \FunctionalTester;

class contactCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function tryToCheckContactPageElements(AcceptanceTester $I)
    {
        $I->amOnPage('/contact-us');
        $I->wantTo('check that the required elements exist on contact page');
        $I->seeElement('#contact-address');
        $I->seeElement('#contact-office-tel');
        $I->seeElement('#contact-email');
        $I->seeElement('#contact-facebook');
        $I->seeElement('#contact-form');
        $I->seeElement('#label-my-name');
        $I->seeElement('#input-my-name');
        $I->seeElement('#label-my-email-address');
        $I->seeElement('#input-my-email-address');
        $I->seeElement('#label-my-phone-number');
        $I->seeElement('#input-my-phone-number');
        $I->seeElement('#label-my-fax-number');
        $I->seeElement('#input-my-fax-number');
        $I->seeElement('#label-product-name');
        $I->seeElement('#input-product-name');
        $I->seeElement('#label-product-code');
        $I->seeElement('#input-product-code');
        $I->seeElement('#label-addtional-comment');
        $I->seeElement('#input-addtional-comment');
        $I->seeElement('#input-submit');
        $I->seeElement('#faq-container');
    }
}