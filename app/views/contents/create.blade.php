@extends('layouts.master')
@section('h1')
Create content
@stop

@section('content')
{{link_to_route('contents.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{ Form::open(array('route' => 'contents.store','method'=>'post','role'=>'form')) }}
<div class="form-group">
    {{Form::label('topic', 'Topic')}}
    @include('includes.error_notification', array('field'=>'topic'))
    {{Form::text('topic','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('text', 'Text')}}
    @include('includes.error_notification', array('field'=>'text'))
    {{Form::textarea('text','',array('class'=>'form-control'))}}
</div>
{{Form::submit('Create',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop