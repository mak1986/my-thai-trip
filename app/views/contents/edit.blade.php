@extends('layouts.master')
@section('h1')
Edit content
@stop

@section('content')
{{link_to_route('contents.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{Form::model($content, array('route' => array('contents.update', $content->id),'method'=>'put','role'=>'form'))}}
<div class="form-group">
    {{Form::label('topic', 'Topic')}}
    @include('includes.error_notification', array('field'=>'topic'))
    {{Form::text('topic',$value = $content->topic,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('text', 'Text')}}
    @include('includes.error_notification', array('field'=>'text'))
    {{Form::textarea('text',$value = $content->text,array('class'=>'form-control'))}}
</div>
{{Form::submit('Edit',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop