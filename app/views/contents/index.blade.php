@extends('layouts.master')

@section('h1')
Contents
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        {{link_to_route('contents.create', "Create a new content", $parameters = array(), $attributes = array('class'=>'btn btn-success'))}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>ID</th><th>Topic</th><th>Text</th><th>Options</th></tr>
            @foreach ($contents as $content)
            <tr>
                <td width="5%">{{$content->id}}</td>
                <td width="15%">{{$content->topic}}</td>
                <td>{{substr($content->text,0,255)."..." }}</td>
                <td width="20%">
                    {{link_to_route('contents.show', "Show", $parameters = array($content->id), $attributes = array('class'=>'btn btn-primary'))}}
                    {{link_to_route('contents.edit', "Edit", $parameters = array($content->id), $attributes = array('class'=>'btn btn-warning'))}}
                    {{Form::open(array('route' => array('contents.destroy', $content->id),'method'=>'delete','style'=>'display:inline'))}}
                    {{Form::submit('Delete',array('class'=>'btn btn-danger','onClick'=>'return (confirm("Are you sure?"))'))}}
                    {{Form::close()}}
                </td>
            </tr>
            @endforeach
        </table>

    </div>
</div>
@stop





