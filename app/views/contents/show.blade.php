@extends('layouts.master')
@section('h1')
{{$content->topic}}
@stop
@section('content')
{{link_to_route('contents.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
<dl class="dl-horizontal">
  <dt>Topic:</dt>
  <dd>{{$content->topic}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Text:</dt>
  <dd>{{$content->text}}</dd>
</dl>
@stop

