@extends('layouts.master')
@section('h1')
Create destination
@stop

@section('content')
{{link_to_route('destinations.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{ Form::open(array('route' => 'destinations.store','method'=>'post','role'=>'form')) }}
<div class="form-group">
    {{Form::label('code', 'Code')}}
    @include('includes.error_notification', array('field'=>'code'))
    {{Form::text('code','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('name', 'Name')}}
    @include('includes.error_notification', array('field'=>'name'))
    {{Form::text('name','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('address', 'Address')}}
    @include('includes.error_notification', array('field'=>'address'))
    {{Form::text('address','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('telephone', 'Telephone')}}
   @include('includes.error_notification', array('field'=>'telephone'))
    {{Form::text('telephone','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('overview', 'Overview')}}
    @include('includes.error_notification', array('field'=>'overview'))
    {{Form::textarea('overview','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('important_info', 'Important Info')}}
   @include('includes.error_notification', array('field'=>'important_info'))
    {{Form::textarea('important_info','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('duration', 'Duration')}}
    @include('includes.error_notification', array('field'=>'duration'))
    {{Form::text('duration','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('price', 'Price')}}
    @include('includes.error_notification', array('field'=>'price'))
    {{Form::text('price','',array('class'=>'form-control'))}}
</div>
{{Form::submit('Create',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop