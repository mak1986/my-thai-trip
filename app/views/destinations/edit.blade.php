@extends('layouts.master')
@section('h1')
Edit destination
@stop

@section('content')
{{link_to_route('destinations.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{Form::model($destination, array('route' => array('destinations.update', $destination->id),'method'=>'put','role'=>'form'))}}
<div class="form-group">
    {{Form::label('code', 'Code')}}
     @include('includes.error_notification', array('field'=>'code'))
    {{Form::text('code',$value=$destination->code,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('name', 'Name')}}
    @include('includes.error_notification', array('field'=>'name'))
    {{Form::text('name',$value=$destination->name,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('address', 'Address')}}
    @include('includes.error_notification', array('field'=>'address'))
    {{Form::text('address',$value=$destination->address,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('telephone', 'Telephone')}}
    @include('includes.error_notification', array('field'=>'telephone'))
    {{Form::text('telephone',$value=$destination->telephone,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('overview', 'Overview')}}
    @include('includes.error_notification', array('field'=>'overview'))
    {{Form::textarea('overview',$value=$destination->overview,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('important_info', 'Important Info')}}
    @include('includes.error_notification', array('field'=>'important_info'))
    {{Form::textarea('important_info',$value=$destination->important_info,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('duration', 'Duration')}}
    @include('includes.error_notification', array('field'=>'duration'))
    {{Form::text('duration',$value=$destination->duration,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('price', 'Price')}}
    @include('includes.error_notification', array('field'=>'price'))
    {{Form::text('price',$value=$destination->price,array('class'=>'form-control'))}}
</div>
{{Form::submit('Edit',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop