@extends('layouts.master')

@section('h1')
Destinations
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        {{link_to_route('destinations.create', "Create a new destination", $parameters = array(), $attributes = array('class'=>'btn btn-success'))}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>Code</th><th>Destination</th><th>Address</th><th>Telephone</th><th>Duration</th><th>Price ($)</th><th>Features</th><th>Options</th></tr>
            @foreach ($destinations as $destination)
            <tr>
                <td width="7%">{{$destination->code}}</td>
                <td width="18%">{{$destination->name}}</td>
                <td width="15%">{{$destination->address}}</td>
                <td width="12%">{{$destination->telephone}}</td>
                <td width="10%">{{$destination->duration}}</td>
                <td width="8%">{{$destination->price}}</td>
                <td width="12%">
                    {{link_to_route('images.relations','Images',$parameters = array($destination->id), $attributes=array('class'=>'btn-link'))}}<br>
                    {{link_to_route('promotions.relations','Promotions',$parameters = array($destination->id), $attributes=array('class'=>'btn-link'))}}
                </td>
                <td width="18%">
                    {{link_to_route('destinations.show', "Show", $parameters = array($destination->id), $attributes = array('class'=>'btn btn-primary'))}}
                    {{link_to_route('destinations.edit', "Edit", $parameters = array($destination->id), $attributes = array('class'=>'btn btn-warning'))}}

                    {{Form::open(array('route' => array('destinations.destroy', $destination->id),'method'=>'delete','style'=>'display:inline'))}}
                    {{Form::submit('Delete',array('class'=>'btn btn-danger','onClick'=>'return (confirm("Are you sure?"))'))}}
                    {{Form::close()}}
                </td>
            </tr>
            @endforeach
        </table>

    </div>
</div>
@stop





