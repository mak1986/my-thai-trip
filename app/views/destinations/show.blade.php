@extends('layouts.master')
@section('h1')
{{$destination->name}}
@stop
@section('content')
{{link_to_route('destinations.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
@include('includes.success_notification')
<p><strong>Images:</strong></p>
@if(sizeof($destination->images)==0)
    <p>You haven't add any images to this destination.</p>
@else
    @foreach($destination->images as $image)
        <img class='thumbnail' src="{{asset("uploads/images/thumbnails/".$image->file)}}" /></a>
    @endforeach
@endif
<hr>
<p><strong>Details:</strong></p>
<dl class="dl-horizontal">
  <dt>Code:</dt>
  <dd>{{$destination->code}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Created at:</dt>
  <dd>{{$destination->created_at}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Updated at:</dt>
  <dd>{{$destination->updated_at}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Name:</dt>
  <dd>{{$destination->name}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Address:</dt>
  <dd>{{$destination->address}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Telephone:</dt>
  <dd>{{$destination->telephone}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Overview:</dt>
  <dd>{{$destination->overview}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Important Info:</dt>
  <dd>{{$destination->important_info}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Duration:</dt>
  <dd>{{$destination->duration}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Price:</dt>
  <dd>{{$destination->price}}</dd>
</dl>
@stop

