<h1>Contact</h1>
<p><strong>Question:</strong> {{$my_question_is_about}}</p>
<p><strong>Name:</strong> {{$name}}</p>
<p><strong>Email:</strong> {{$email}}</p>
<p><strong>Telephone:</strong> {{$telephone}}</p>
<p><strong>Travel date:</strong> {{$travel_date}}</p>
<p><strong>Product code:</strong> {{$product_code}}</p>
<p><strong>Comment:</strong> {{$comment}}</p>
