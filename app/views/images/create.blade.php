@extends('layouts.master')
@section('h1')
Create image
@stop

@section('content')
{{link_to_route('images.relations', "Back", $parameters = array($destinations_id), $attributes = array('class'=>'btn btn-default'))}}
<hr>

{{Form::open(array('route' => 'images.store','method'=>'post','role'=>'form','files'=>true)) }}
<div class="form-group">
    {{Form::label('file', 'Photo')}}
    @include('includes.error_notification', array('field'=>'file'))
    {{Form::file('file',array('class'=>'form-control'))}}
    {{Form::hidden('destinations_id',$destinations_id);}}
</div>
{{Form::submit('Upload',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop