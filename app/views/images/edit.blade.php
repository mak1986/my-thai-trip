@extends('layouts.master')
@section('h1')
Edit image
@stop

@section('content')
{{link_to_route('destinations.show', "Back", $parameters = array($destination_id), $attributes = array('class'=>'btn btn-default'))}}
<hr>

{{Form::open(array('route' => 'images.store','method'=>'post','role'=>'form','files'=>true))}}
<div class="form-group">
    {{Form::label('file', 'Photo')}}
    @include('includes.error_notification', array('field'=>'file'))
    {{Form::file('file',array('class'=>'form-control'))}}
    {{Form::hidden('destination_id',$destination_id);}}
</div>
{{Form::submit('Upload',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop