@extends('layouts.master')
@section('h1')
Images for {{$destination->name}} ({{$destination->code}})
@stop
@section('content')
{{link_to_route('destinations.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
@include('includes.success_notification')
<div class="row">
    <div class="col-lg-12">
        {{link_to_route('images.create', "Add a photo", $parameters = array('destinations_id'=>$destination->id), $attributes = array('class'=>'btn btn-success'))}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        <p><strong>Images:</strong></p>
@if(sizeof($images)==0)
    <p>You haven't add any images to this destination.</p>
@else
    @foreach($images as $image)
    <a href='{{ route('images.show', $parameters = array($image->id))}}'><img class='thumbnail' src="{{asset("uploads/images/thumbnails/".$image->file)}}" /></a>
    @endforeach
@endif
    </div>
</div>

@stop

