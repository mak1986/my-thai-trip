@extends('layouts.master')
@section('h1')
Image
@stop
@section('content')
{{link_to_route('images.relations', "Back", $parameters = array($image->destinations_id), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{Form::open(array('route' => array('images.destroy', $image->id),'method'=>'delete'))}}
{{Form::submit('Delete',array('class'=>'btn btn-danger','onClick'=>'return (confirm("Are you sure?"))'))}}
{{Form::close()}}
<hr>
 <p><strong>Thumbnail:</strong></p>
<img class='thumbnail' src='{{asset('uploads/images/thumbnails/'.$image->file)}}' />
<br><br>
<p><strong>Medium size:</strong></p>
<img class='thumbnail' src='{{asset('uploads/images/mediumsize/'.$image->file)}}' />
<br><br>
<p><strong>Full size:</strong></p>
<img class='thumbnail' src='{{asset('uploads/images/fullsize/'.$image->file)}}' />
<br><br>
<p><strong>Original:</strong></p>
<img class='thumbnail' src='{{asset('uploads/images/original/'.$image->file)}}' />
@stop

