<div id="cart-container" class="follow-scroll">
    <h3>CART</h3>
    <hr>
    @if(!$cart->isEmpty())  

<?php $cart = unserialize(serialize($cart)); ?>
    <p>TRAVEL DATE: {{$cart->getFormatedTravelDate()}}</p>
    <hr>
<?php //echo "<pre>";var_dump($cart); die(); ?>
    <table width='100%'>

        @foreach($cart->getItems() as $item)

        <tr><td>{{$item->countTourists()}} x </td><td><a href='#update-to-cart-{{$item->getId()}}' class='update-to-cart-window'>{{strtoupper($item->getName())}}</a></td><td><div class='cart-destination-total'>${{$item->getTotalPrice()}}</div></td><td><a href='{{route('travellers.delete',array($item->getId()))}}' class='delete-destination'><img src='{{asset('uploads/images/commons/trash.png')}}'></a></td></tr>
        @endforeach
    </table>
    <hr>
    <div id='cart-total'>TOTAL ${{$cart->getTotalPrice()}}</div>
    <a href='{{url('payments/create')}}' class='btn btn-checkout'>CHECKOUT</a>
    <!--<pre>{{var_dump(Session::get('cart'))}}</pre>-->
    @else
    <p>Your cart is empty</p>
    @endif
</div>
<script>
    $(function () {

        var $sidebar = $(".follow-scroll"),
                $window = $(window),
                offset = $sidebar.offset(),
                topPadding = 50;

        $window.scroll(function () {
            if ($window.scrollTop() > offset.top) {
                $sidebar.stop().animate({
                    marginTop: $window.scrollTop() - offset.top + topPadding
                });
            } else {
                $sidebar.stop().animate({
                    marginTop: -25
                });
            }
        });

    });
</script>

