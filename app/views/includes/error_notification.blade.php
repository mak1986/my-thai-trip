@if($errors->has($field))
<div class="alert alert-danger alert-dismissable">
    <p>{{ $errors->first($field) }}</p>
</div>
@endif