@if (Session::has('success_message'))
<div class="alert alert-success alert-dismissable">
    <p>{{ Session::get('success_message') }}</p>
</div>
@endif