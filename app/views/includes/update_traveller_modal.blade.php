@if(!$cart->isEmpty())
@foreach($cart->getItems() as $item)
<div id="update-to-cart-{{$item->getId()}}" class="update-to-cart-popup">

    <a href="#" class="close"><img src="{{asset('uploads/images/commons/close_pop.png')}}" class="btn_close" title="Close Window" alt="Close" /></a>
    <div class="popup-header" class="center bold">
        {{$item->getName()}}
    </div>

    {{ Form::open(array('route' => 'travellers.store','class'=>'add-to-cart-form','method'=>'post','role'=>'form')) }}

    <div class='error'></div>
    @include('includes.error_notification', array('field'=>'content'))
    <div class="travel-date form-group">
        {{Form::label('travel_date','Travel date:',array('class'=>'travel-date-label'))}}
        {{Form::text('travel_date',$cart->getTravelDate(),array('required'=>'required','id'=>'travel-date-'.$item->getId(),'class'=>'input-text form-control'))}}
    </div><br>
    <p class='center'>Traveller(s)</p>
    <!-- Squared TWO -->
    <ul id='travellers-list'>
        <li><input type="checkbox" class="select-all-checkbox" /> <label class="select-all-label">Select all</label></li>     
        @if(!$cart->isEmpty())
        <?php $first = true; ?>
        @foreach($cart->getTourists() as $tourist)
        <li>
            <?php $tourist = unserialize(serialize($tourist)); ?>
            @if($tourist->hasItemId($item->getId()))
            <input type="checkbox" name="check[{{$tourist->getId()}}]" checked="checked" value='cart|{{$tourist->getId()}}'/>
            @else
            <input type="checkbox" name="check[{{$tourist->getId()}}]"  value='cart|{{$tourist->getId()}}'/>
            @endif
            {{Form::text('first_name['.$tourist->getId().']',$tourist->getFirstName(),array('required'=>'required','class'=>'name first-name input-text form-control','placeholder'=>'First name'))}}

            {{Form::text('last_name['.$tourist->getId().']',$tourist->getLastName(),array('required'=>'required','class'=>'name last-name input-text form-control','placeholder'=>'Last name'))}}

            {{Form::number('age['.$tourist->getId().']',$tourist->getAge(),array('required'=>'required','class'=>'number age input-text form-control','placeholder'=>'Age','max'=>100,'min'=>1))}}
            @if(!$first)
            <img width="25" height="25" onClick="javascript:deleteTraveller()" class="delete-btn" src="http://localhost/tourism/public/uploads/images/commons/delete.png" alt="Home" />
            @endif
            <?php $first = false; ?>

            {{Form::hidden('id['.$tourist->getId().']','cart|'.$tourist->getId(),array('id'=>$tourist->getId()))}}
        </li>
        @endforeach
        @endif

    </ul>
    <div id="add-traveller" onClick='javascript:addTraveller()' class="round-button">
        <img src="{{asset('uploads/images/commons/plus.png')}}" alt="Home" />
    </div>
    <hr>
    <table class="add-traveller-note">
        <tr><td width='10%'><strong>Note: </strong></td><td width='90%'>Half Price for children (Age 1–12)!</td></tr>
        <tr><td></td><td>Only travellers with the checkboxes checked will join this destination.</td></tr>
        <tr><td></td><td>Deleted traveller will be removed from every destinations.</td></tr>
    </table>



    {{Form::hidden('destination_id',$item->getId())}}
    <div class='center'>
        {{Form::submit('UPDATE CART',array('class'=>'btn btn-add-to-cart'))}}
    </div>
    {{ Form::close() }}

</div>
@endforeach
@endif
<script type='text/javascript'>
    $(document).ready(function () {
        $('a.add-to-cart-window,a.update-to-cart-window').click(function () {

            // Getting the variable's value from a link 
            var loginBox = $(this).attr('href');
            //Fade in the Popup and add close button
            $(loginBox).fadeIn(300);
            //Set the center alignment padding + border
            var popMargTop = ($(loginBox).height() + 24) / 2;
            var popMargLeft = ($(loginBox).width() + 24) / 2;
            $(loginBox).css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });
            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);
            return false;
        });
        // When clicking on the button close or the mask layer the popup closed
        $('a.close, #mask').live('click', function () {
            $('#mask , .update-to-cart-popup').fadeOut(300, function () {
                $('#mask').remove();
            });
            return false;
        });

    });
    function addTraveller() {
        var number = 1 + $("#travellers-list li:last input:last").attr('id') / 1;
        $("#travellers-list li:last-child").after('<li>'
                + '<input type="checkbox" value="new|' + number + '" name="check[' + number + ']" checked="checked"/> '
                + '<input required class="name first-name input-text form-control" placeholder="First name" name="first_name[' + number + ']" type="text" value=""> '
                + '<input required class="name last-name input-text form-control" placeholder="Last name" name="last_name[' + number + ']" type="text" value=""> '
                + '<input required class="number age input-text form-control" placeholder="Age" max="100" min="1" name="age[' + number + ']" type="number" value=""> '
                + '<input id="' + number + '" name="id[' + number + ']" type="hidden" value="new|' + number + '" />'
                + '<img width="25" height="25" onClick="javascript:deleteTraveller()" class="delete-btn" src="http://localhost/tourism/public/uploads/images/commons/delete.png" alt="Home" /> '
                + '</li>');
    }
    function deleteTraveller($id) {
        $($id).remove();
    }
    $('li .delete-btn').live('click', function (e) {
        $(e.target.parentNode).remove();
    });
    @foreach($cart->getItems() as $item)
        <?php $str = "'#travel-date-".$item->getId()."'";?>
    $({{$str}}).datepicker({
        dateFormat: "yy-mm-dd",
        weekStart: 1,
        calendarWeeks: true,
        todayHighlight: true,
        minDate: 7
    });
    @endforeach
    $('.name').rules("add", {required: true});
    $('.number').rules("add", {required: true, number: true});
    $(".add-to-cart-form").validate({
        errorLabelContainer: $(".add-to-cart-form div.error"),
        messages: {
            travel_date: {
                required: 'Travel date is required!<br>'
            },
            "first_name[]": {
                required: 'First Name fields are required!<br>'
            },
            "last_name[]": {
                required: 'Last Name fields are required!<br>'
            },
            "age[]": {
                required: 'Age fields are required and must be a number!<br>'
            }
        }

    });

</script>