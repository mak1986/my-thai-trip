<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>My Thai Trip - Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="{{asset('bootstrap/css/plugins/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{asset('bootstrap/css/sb-admin-2.css')}}" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="{{asset('bootstrap/font-awesome-4.1.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

        <!-- Datepicker styles -->
        <link href="{{asset('bootstrap/css/datepicker3.css')}}" rel="stylesheet" type="text/css">

        <!-- Custom styles -->
        <link href="{{asset('css/admin-style.css')}}" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('contents')}}">My Thai Trip - Admin</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{url('home')}}"> Frontend</a>
                            </li>
                            <!--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>-->
                            <li>
                                <a href="{{url('logout', $parameters = array(), $secure = null)}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="{{url('contents', $parameters = array(), $secure = null)}}"><i class="fa fa-edit fa-fw"></i> Contents</a>
                            </li>
                            <li>
                                <a href="{{url('destinations', $parameters = array(), $secure = null)}}"><i class="fa fa-edit fa-fw"></i> Destinations</a>
                            </li>
                            <li>
                                <a href="{{url('newsletters', $parameters = array(), $secure = null)}}"><i class="fa fa-edit fa-fw"></i> Newsletters</a>
                            </li>
                            <li>
                                <a href="{{url('travellers', $parameters = array(), $secure = null)}}"><i class="fa fa-edit fa-fw"></i> Travellers</a>
                            </li>
                            <li>
                                <a href="{{url('reviews', $parameters = array(), $secure = null)}}"><i class="fa fa-edit fa-fw"></i> Reviews</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">@yield('h1')</h1>
                            @yield('content')

                        </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="{{asset('bootstrap/js/jquery.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{asset('bootstrap/js/plugins/metisMenu/metisMenu.min.js')}}"></script>

        <!-- Custom Theme JavaScript -->
        <script src="{{asset('bootstrap/js/sb-admin-2.js')}}"></script>

        <!-- Datepicker JavaScript -->
        <script src="{{asset('bootstrap/js/bootstrap-datepicker.js')}}"></script>


        <script type="text/javascript">
            $('#from_date').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                calendarWeeks: true,
                todayHighlight: true
            });
            $('#to_date').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                calendarWeeks: true,
                todayHighlight: true
            });
        </script>
    </body>

</html>