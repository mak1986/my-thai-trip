<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="{{asset('css/reset.css')}}" rel="stylesheet">

        <!-- jquery CSS -->
        <link href="{{asset('js/jquery-ui-1.11.2.custom/jquery-ui.css')}}" rel="stylesheet">
        <link href="{{asset('js/jquery-ui-1.11.2.custom/jquery-ui.structure.css')}}" rel="stylesheet">
        <link href="{{asset('js/jquery-ui-1.11.2.custom/jquery-ui.theme.css')}}" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Bevan' rel='stylesheet' type='text/css'>
          <link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
        <link href="{{asset('js/slider/slider.css')}}" rel="stylesheet">
        <link href="{{asset('js/qaptcha/QapTcha.jquery.css')}}" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <!-- jQuery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <!--<script src="{{asset('js/jquery-ui-1.11.2.custom/external/jquery/jquery.js')}}"></script>-->
        <script type='text/javascript' src="{{asset('js/jquery-ui.js')}}"></script>
        <script type='text/javascript' src="{{asset('js/jquery-ui-1.11.2.custom/jquery-ui.js')}}"></script>
        <script type='text/javascript' src="{{asset('js/slider/slider.js')}}"></script>
        <script type='text/javascript' src="{{asset('js/slider/initslider-1.js')}}"></script>

    </head>

    <body>

        <div id="wrapper">
            <div id='header'>
                <div id='header-container'>
                    <div id='logo'>
                        <a href='{{url('/home')}}'>My Thai Trip</a>
                    </div>
                    <div id="menu">
                        <a href="{{url('/home')}}">HOME</a> 
                        <a href="{{url('/customize-tour')}}">CUSTOMIZE TOUR</a>
                        <a href="{{url('/why-choose-us')}}">WHY CHOOSE US</a>
                        <a href="{{url('/about-us')}}">ABOUT US</a> 
                        <a href="{{url('/contact-us')}}">CONTACT US</a>  
                    </div>
                    <div class='clear'></div>
                </div>

            </div>
            <div id='banner'>

            </div>
            
            <div  id='main-container'>
                @yield('content')
            </div>

            <div id='footer'>
                <div id='newsletter-container'>

                    {{Form::open(array('route' => 'newsletter_registrations.store','class'=>'newsletter-registration-form test','method'=>'post','role'=>'form')) }}
                    @include('includes.error_notification',array('field'=>'email'))
                    {{Form::label('email','Subscribe to our email newsletter:')}}
                    {{Form::text('email','',array('required'=>'required','class'=>'input-text','placeholder'=>'Your email address'))}}
                    {{Form::submit('SIGN UP',array('class'=>'btn btn-newsletter-sign-up'))}}
                    {{Form::close()}}
                </div>
                <div id='footer-container'>
                    <div class='footer-block' id='footer-contact'>
                        <p class='bold'>Contact:</p>
                        <p>106/57 M.7 T.Nonghoi A.Muang Chiang Mai 50000 Thailand, Tel. 087-5805292.</p>

                    </div>
                    <div class='footer-block' id='payments'>
                        <p class='bold'>Payments:</p>
                        <p><img src='{{asset('uploads/images/commons/visa.png')}}'/> Visa</p>
                        <p><img src='{{asset('uploads/images/commons/maestrocard.png')}}'/> Maestrocard</p>
                        <p><img src='{{asset('uploads/images/commons/mastercard.png')}}'/> Mastercard (Debit & Credit)</p>
                    </div>
                    <div class='footer-block' id='help'>
                        <p class='bold'>Help:</p>
                        <p><a href="{{url('/faq')}}">FAQ</a></p> 
                        <p><a href="{{url('/terms')}}">Terms</a></p> 
                        <p><a href="{{url('/privacy-policy')}}">Privacy Policy</a></p>
                    </div>
                    <div class='footer-block' id='follow-us'>
                        <p class='bold'>Follow us on:</p>
                        <p><a href='#'>Facebook</a></p>
                        <p><a href='#'>Twitter</a></p>
                    </div>
                    <div class='clear'></div>
                    <div id='bottom'>
                        <p class='center'>My Thai Trip is Chiang Mai travel agency in Northern Thailand, we arrange the tours in Chiang Mai, travel and trekking in Chiang Mai,
                            package tours and daily tours, Chiang Mai tours, Chiang Mai travel</p>
                        <p class='center' id='copyright'>© 2014 My Thai Trip | <a id='admin-btn' href='{{url('login')}}'>Admin</a></p>
                    </div>
                </div>
            </div>

        </div>        <script src="{{asset('js/validate/jquery.validate.js')}}"></script>
        <script type='text/javascript'>



    $(".newsletter-registration-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: 'Email is required!'
            }
        }

    });

        </script>

        @yield('script')

    </body>

</html>