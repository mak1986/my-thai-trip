@extends('layouts.master')
@section('h1')
Create content
@stop

@section('content')
{{link_to_route('newsletters.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{ Form::open(array('route' => 'newsletters.store','method'=>'post','role'=>'form')) }}
<div class="form-group">
    {{Form::label('topic', 'Topic')}}
    @include('includes.error_notification', array('field'=>'topic'))
    {{Form::text('topic','',array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('content', 'Content')}}
    @include('includes.error_notification', array('field'=>'content'))
    {{Form::textarea('content','',array('class'=>'form-control'))}}
</div>
{{Form::submit('Create',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop