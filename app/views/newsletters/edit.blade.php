@extends('layouts.master')
@section('h1')
Edit newsletter
@stop

@section('content')
{{link_to_route('newsletters.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
{{Form::model($newsletter, array('route' => array('newsletters.update', $newsletter->id),'method'=>'put','role'=>'form'))}}
<div class="form-group">
    {{Form::label('topic', 'Topic')}}
    @include('includes.error_notification', array('field'=>'topic'))
    {{Form::text('topic',$value = $newsletter->topic,array('class'=>'form-control'))}}
</div>
<div class="form-group">
    {{Form::label('content', 'Content')}}
    @include('includes.error_notification', array('field'=>'content'))
    {{Form::textarea('content',$value = $newsletter->content,array('class'=>'form-control'))}}
</div>
{{Form::submit('Edit',array('class'=>'btn btn-primary'))}}
{{ Form::close() }}
@stop