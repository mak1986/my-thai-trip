@extends('layouts.master')

@section('h1')
Newsletters
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        {{link_to_route('newsletters.create', "Create a new newsletter", $parameters = array(), $attributes = array('class'=>'btn btn-success'))}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>Topic</th><th>Content</th><th>Options</th></tr>
            @foreach ($newsletters as $newsletter)
            <tr>
                <td width="15%">{{$newsletter->topic}}</td>
                <td>{{substr($newsletter->content,0,255)."..." }}</td>
                <td width="30%">
                    {{link_to_route('newsletters.send', "Send newsletter", $parameters = array($newsletter->id), $attributes = array('class'=>'btn btn-default'))}}
                    {{link_to_route('newsletters.show', "Show", $parameters = array($newsletter->id), $attributes = array('class'=>'btn btn-primary'))}}
                    {{link_to_route('newsletters.edit', "Edit", $parameters = array($newsletter->id), $attributes = array('class'=>'btn btn-warning'))}}
                    
                    {{Form::open(array('route' => array('newsletters.destroy', $newsletter->id),'method'=>'delete','style'=>'display:inline'))}}
                    {{Form::submit('Delete',array('class'=>'btn btn-danger','onClick'=>'return (confirm("Are you sure?"))'))}}
                    {{Form::close()}}
                </td>
            </tr>
            @endforeach
        </table>

    </div>
</div>
@stop





