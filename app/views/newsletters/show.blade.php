@extends('layouts.master')
@section('h1')
{{$newsletter->topic}}
@stop
@section('content')
{{link_to_route('newsletters.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<hr>
<dl class="dl-horizontal">
  <dt>Topic:</dt>
  <dd>{{$newsletter->topic}}</dd>
</dl>
<dl class="dl-horizontal">
  <dt>Content:</dt>
  <dd>{{$newsletter->content}}</dd>
</dl>
@stop

