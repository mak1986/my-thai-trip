@extends('layouts.page')
@section('content')
<h1>{{strtoupper($content->topic)}}</h1>
<hr>
<div class='content'><p>{{nl2br($content->text)}}</p></div>
<br><br>
<h2>If you have any questions please feel free to contact us.</h2>
{{ Form::open(array('route' => 'contact_us.submitContact','class'=>'contact-form','method'=>'post','role'=>'form')) }}
<div class='form-group'>
    {{Form::label('my_question_is_about','My question is about:')}}
    {{Form::text('my_question_is_about','',array('placeholder'=>'Your question','required'=>'required','class'=>'input-text form-control'))}}
</div>
<div class='form-group'>
    {{Form::label('name','Name:')}}
    {{Form::text('name','',array('placeholder'=>'Your name','required'=>'required','class'=>'input-text form-control'))}}
</div>
<div class='form-group'>
    {{Form::label('email','Email:')}}
    {{Form::text('email','',array('placeholder'=>'Your email address','required'=>'required','class'=>'input-text form-control'))}}
</div>
<div class='form-group'>
    {{Form::label('telephone','Telephone:')}}
    {{Form::text('telephone','',array('placeholder'=>'Your telephone number','required'=>'required','class'=>'input-text form-control'))}}
</div>
<div class='form-group'>
    {{Form::label('travel_date','Travel date:')}}
    {{Form::text('travel_date','',array('placeholder'=>'Your travel date','required'=>'required','id'=>'travel-date','class'=>'input-text form-control','onClick'=>'javascript:pickTravelDate(this)'))}}
</div>
<div class='form-group'>
    {{Form::label('product_code','Product Code:')}}
    {{Form::text('product_code','',array('placeholder'=>'Our product code','required'=>'required','class'=>'input-text form-control'))}}
</div>
<div class='form-group'>
    {{Form::label('comment','Additional comments:')}}<br>
    {{Form::textarea('comment','',array('cols'=>'114','required'=>'required','class'=>'input-textear form-control'))}}
</div>

{{Form::submit('SEND',array('class'=>'btn btn-send-contact'))}}
{{ Form::close() }}
@stop

@section('script')
<script>
    $('#travel-date').datepicker({
        dateFormat: "yy-mm-dd",
        weekStart: 1,
        calendarWeeks: true,
        todayHighlight: true,
        minDate: 7
    });
    $(".contact-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            
        }
    });
</script>
@stop