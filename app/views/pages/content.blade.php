@extends('layouts.page')
@section('content')
<h1>{{strtoupper($content->topic)}}</h1>
<hr>
<div class='content'><p>{{nl2br($content->text)}}</p></div>
@stop