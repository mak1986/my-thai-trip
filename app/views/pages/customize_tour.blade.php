@extends('layouts.product')
@section('content')
@include('includes.update_traveller_modal')
@include('includes.cart')
<h1>{{strtoupper($content->topic)}}</h1>
<hr>
<div><p>{{nl2br($content->text)}}</p></div>
<!-- promotions -->
<div>
    <style>
        .item-card {
            margin:20px;
            float:left;
            width: 440px;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            -ms-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
            padding-bottom: 1.5em;
            background-color: #dde1e2;
            -moz-box-shadow: 0px 0px 7px #000000;
            -webkit-box-shadow: 0px 0px 7px #000000;
            box-shadow: 0px 0px 7px #000000;
        }
        .item-card .header {
            display: block;
            overflow: hidden;
            position: relative;
            /* padding-bottom: 2em; */
            -moz-border-radius-topleft: 8px;
            -webkit-border-top-left-radius: 8px;
            border-top-left-radius: 8px;
            -moz-border-radius-topright: 8px;
            -webkit-border-top-right-radius: 8px;
            border-top-right-radius: 8px;
        }
        .item-card .bio:hover > .desc {
            cursor: pointer;
            opacity: 1;
        }
    </style>

</div>

</div>
<!-- all destinations-->
<div id="items-container">
    <ul>


        @foreach($promotions as $promotion)
        <li class="item-card">
            <div class="header">
                <div onClick="goToProduct({{$promotion->destination->id}})" class="bio">

                    @if(count($promotion->destination->images)>0)
                    <img src="{{asset("uploads/images/bigsize/".$promotion->destination->images[0]->file)}}" class="bg"/>
                    @endif
                    <div class="desc">
                        <h4>{{ucwords($promotion->destination->name)}}</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam temporibus magnam maiores sint numquam odit</p>
                    </div>

                </div>

            </div>
            <div class="content">
                <div class="ribbon"><div class="ribbon-stitches-left"></div><div class="ribbon-stitches-right"></div><div class="ribbon-stitches-top"></div><strong class="ribbon-content"><h1>{{$promotion->discount}}% Off! Now only $ {{number_format($promotion->destination->price-($promotion->destination->price*($promotion->discount/100)),2,'.','')}}</h1></strong><div class="ribbon-stitches-bottom"></div></div>

                <div class="data">
                    <ul>
                        <li>
                            <?php 
                            $c = 0;
                            foreach($promotion->destination->reviews as $review){
                                if($review->status==1)
                                    $c++;
                            }?>
                             {{$c}}
                            <span>Review(s)</span>
                        </li>
                        <li>
                            {{$promotion->destination->duration}}
                            <span>Duration</span>
                        </li>
                        <li>
                            <b style='text-decoration:line-through'>$ {{number_format($promotion->destination->price,2,'.','')}}</b>

                            <span>Price</span>
                        </li>
                    </ul>
                </div>
            </div>

        </li>
        @endforeach

        @foreach($destinations as $destination)
        <li class="item-card">
            <div class="header">
                <div onClick="goToProduct({{$destination->id}})" class="bio">
                    @if(count($destination->images)>0)
                    <img src="{{asset("uploads/images/bigsize/".$destination->images[0]->file)}}" class="bg"/>
                    @endif
                    <div class="desc">
                        <h4>{{ucwords($destination->name)}}</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam temporibus magnam maiores sint numquam odit</p>
                    </div>

                </div>
            </div>
            <div class="content">
                <div class="data">
                    <ul>
                        <li>
                            <?php 
                            $c = 0;
                            foreach($destination->reviews as $review){
                                if($review->status==1)
                                    $c++;
                            }?>
                            {{$c}}
                            <span>Review(s)</span>
                        </li>
                        <li>
                            {{$destination->duration}}
                            <span>Duration</span>
                        </li>
                        <li>
                            $ {{number_format($destination->price,2,'.','')}}
                            <span>Price</span>
                        </li>
                    </ul>
                </div>
            </div>

        </li>
        @endforeach
        <li class="clear"></li>
    </ul>
    <script>
                function goToProduct(id){
                window.location.assign("{{url('customize-tour/product/')}}/" + id);
                }
    </script>

    @stop