@extends('layouts.product')

@section('content')
@include('includes.update_traveller_modal')
@include('includes.cart')
@include('includes.add_traveller_modal')

<h1>{{strtoupper($destination->name)}}</h1>
<hr>

<div id='destination-header'>
    <div id='destination-header-slide'>
        <!-- Insert to your webpage where you want to display the slider -->
        <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:910px;margin:0 auto 56px;">
            <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                <ul class="amazingslider-slides" style="display:none;">
                    @foreach($destination->images as $image)
                    <li><img src="{{asset('uploads/images/fullsize/'.$image->file)}}" /></li>
                    @endforeach        
                </ul>
                <ul class="amazingslider-thumbnails" style="display:none;">

                    @foreach($destination->images as $image)
                    <li><img src="{{asset('uploads/images/thumbnails/'.$image->file)}}" /></li>
                    @endforeach   
                </ul>
            </div>
        </div>
        <!-- End of body section HTML codes -->
    </div>
    <div id='destination-detail-bg'></div>
    <div id='destination-detail'>
        @if($promotion)
        <div class="ribbon ribbon-product"><div class="ribbon-stitches-left"></div><div class="ribbon-stitches-right"></div><div class="ribbon-stitches-top"></div><strong class="ribbon-content"><h1>{{$promotion->discount}}% Off! Now only $ {{number_format($promotion->destination->price-($promotion->destination->price*($promotion->discount/100)),2,'.','')}}</h1></strong><div class="ribbon-stitches-bottom"></div></div>
        @endif
        <ul>
            <li id='name-and-code' class='<?php echo ($promotion)? 'promotion':''; ?>'>
                <p><strong>{{$destination->name}}</strong></p>
                <p id='product-code'>Product code: {{$destination->code}}</p>
            </li>
            <li id='buy-button'>
                @if($cart->hasItem($destination->id))
                <div id='add-to-cart-btn' class="btn-sign" style='display:inline'>
                    <a href="#update-to-cart-{{$destination->id}}" class='update-to-cart-window'>Edit</a>
                </div>
                @else
                <div id='add-to-cart-btn' class="btn-sign" style='display:inline'>
                    <a href="#add-to-cart-box" class="add-to-cart-window">Add to cart</a>
                </div>
                @endif
            </li>
            <li id='duration'>
                <p>{{$destination->duration}}</p>
                <p id='duration-text'>DURATION</p>
            </li>
            @if($promotion)
            

            <li class='price'>
                <p style="text-decoration: line-through">$ {{number_format($destination->price, 2, '.','')}}</p>
                <p id='price-text'>PRICE</p>
            </li>
            <li class='price'>
                <p>$ {{number_format($destination->price-($destination->price*$promotion->discount)/100, 2, '.','')}}</p>
                <p id='price-text'>NEW PRICE</p>
            </li>
            @else
            <li class='price'>
                <p>$ {{number_format($destination->price, 2, '.','')}}</p>
                <p id='price-text'>PRICE</p>
            </li>
            @endif

        </ul>
    </div>
    <div class='clear'></div>
</div>

<div id='destination-footer'>
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Overview</a></li>
            <li><a href="#tabs-2">Important Info</a></li>
        </ul>
        <div id="tabs-1">
            <p>({{nl2br($destination->overview)}}</p>
        </div>
        <div id="tabs-2">
            <p>{{nl2br($destination->important_info)}}</p>
        </div>
    </div>
</div>
<div class="reviews-container">
@include('reviews.show',['reviews'=>$reviews])
@include('reviews.create',['destination_id'=>$destination->id])
</div>
@stop

@section('script')
<script src='{{asset('js/blur/blur.js')}}'></script>
<script type='text/javascript'>
        $(function () {
            $("#tabs").tabs();
        });
</script>

@stop


