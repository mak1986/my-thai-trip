@extends('layouts.page')
@section('content')
<h1>Your Order</h1>
<hr>
<div id='invoice-container' style="padding:20px;background-color:#F2F2F2;font-family: 'Open Sans', sans-serif;font-size:13px;line-height: 18px;">
    <table class='invoice-table' witdh="100%">
        <tr>
            <td colspan="4"><span class="logo">My Thai Trip</span></td>
            <td colspan="4" style="text-align: right"><span style="font-size:30px;">INVOICE</span></td>
        </tr>
        <tr>
            <td class='info' width="25%" colspan="2">106/57 M.7 T.Nonghoi A.Muang Chiang Mai 50000 Thailand<br><br></td>
            <td width="13%"></td>
            <td width="12%"></td>   
            <td width="13%"></td>
            <td width="12%"></td>
            <td width="13%"></td> 
            <td width="12%"></td>  
        </tr>
        <tr>
            <td>Tel.:</td>
            <td>087-5805292</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Date:</td>
            <td>{{(new DateTime())->format('d M Y')}}</td>

        </tr>
        <tr>
            <td>Fax:<br></td>
            <td>087-5805215</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Invoice No:</td>
            <td>{{sprintf('%05d', $cart->getOrderId())}}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Travel Date:</td>
            <td>{{$cart->getFormatedTravelDate()}}</td>
        </tr>
    </table>
    <br>
    @if(!$cart->isEmpty())
    <table class='invoice-table' width="100%">
        <tr>
            <th style="text-align: left">Product Code</th>
            <th style="text-align: left">Product</th>
            <th style="text-align: left">Quantity</th>
            <th style="text-align: right">Unit Price</th>
            <th style="text-align: right">Row Total</th>
        </tr>
        <?php $count = 0; ?>
        @foreach($cart->getItems() as $item)
        <tr>
            <td style="text-align: left">{{$item->getCode()}}</td>
            <td style="text-align: left">{{$item->getName()}}</td>
            <td style="text-align: left">
                <?php
                $countAdults = $item->countAdults();
                $countChildren = $item->countChildren();
                ?>
                @if($countAdults > 0)
                {{$countAdults}} x Adult(s)
                @endif
                @if($countChildren > 0)
                <br>{{$countChildren}} x Child(ren)
                @endif
            </td>
            <?php
            $str = "";
            if ($countAdults > 0)
                $str .= "$ {$item->getPrice()}<br>";
            if ($countChildren > 0)
                $str .= "$ {$item->getChildPrice()}";
            ?>
            <td style="text-align: right">{{$str}}</td>
            <?php
            $str = "";
            if ($countAdults > 0)
                $str .= "$ {$item->getTotalPriceAdult()}<br>";
            if ($countChildren > 0)
                $str .= "$ {$item->getTotalPriceChild()}";
            ?>
            <td style="text-align: right">{{$str}}</td>
        </tr>
        <?php $count++; ?>
        @endforeach

        @if($count<8)
        <?php
        for ($i = 0; ($i + $count) < 8; $i++) {
            ?>
            <tr><td><br></td><td></td><td></td><td></td><td></td></tr>
        <?php } ?>
        @endif
        <tr><td class="invoice-summary"></td><td class="invoice-summary"></td><td class="invoice-summary"></td><td class="invoice-summary" style="text-align: right"><br>Subtotal:</td><td class="invoice-summary" style="text-align:right"><br>$ {{$cart->getTotalPrice()}}</td></tr>
        <tr><td></td><td></td><td></td><td style="text-align: right">VAT Rate:</td><td style="text-align:right">7%</td></tr>
        <tr><td></td><td></td><td></td><td style="text-align: right">VAT:</td><td style="text-align:right">$ {{$cart->calculateVat($cart->getTotalPrice())}}</td></tr>
        <tr><td></td><td></td><td></td><td style="font-weight:bold;text-align: right;">Total:</td><td style="text-align:right;font-weight: bold">$ {{$cart->getTotalPrice()+$cart->calculateVat($cart->getTotalPrice())}}</td></tr>
    </table>

    @else
    <p>No items in your cart...</p>
    @endif
</div>
<div class='center'>
    <button class='center btn btn-download-invoice'>Download</button>
</div>
<div id='payment-container'>
    <h2>Payment</h2>

    <hr>
    {{ Form::open(array('route' => 'payments.store','class'=>'payment-form','method'=>'post','role'=>'form')) }}
    @if(!$errors->isEmpty())
    <div class="alert alert-danger alert-dismissable">
        <p style='font-weight: bold'>Ooops, there are some errorsin the form.</p>
        @foreach ($errors->all('<p>:message</p>') as $message)
        {{ $message}}</p>
        @endforeach
    </div>
    @endif
    <table width='100%'>
        <tr class='form-group'>
            <td width='25%'>{{Form::label('name','Cardholder name:')}}</td>
            <td style='text-align: right' width='75%'>{{Form::text('name','',array('required'=>'required','placeholder'=>'Cardholder name','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('email','Email:')}}</td>
            <td style='text-align: right'>{{Form::text('email','',array('required'=>'required','placeholder'=>'Your current email address','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('billing_address_line_1','Billing address:')}}</td>
            <td style='text-align: right'>{{Form::text('billing_address_line_1','',array('required'=>'required','placeholder'=>'Your billing address','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td></td>
            <td style='text-align: right'>{{Form::text('billing_address_line_2','',array('placeholder'=>'Your billing address','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('city','City:')}}</td>
            <td style='text-align: right'>{{Form::text('city','',array('required'=>'required','placeholder'=>'Your city','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('postal_code','Postal code:')}}</td>
            <td style='text-align: right'>{{Form::text('postal_code','',array('required'=>'required','placeholder'=>'Your postal code','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('country','Country:')}}</td>
            <td style='text-align: right'>{{Form::text('country','',array('required'=>'required','placeholder'=>'Your country','class'=>'input-text'))}}</td>
        </tr>
        <tr><td>Payment methods:</td><td style="text-align: right;vertical-align: middle"><div style='float:right;width:313px'><img style='float: left;margin-bottom:5px;' height="30px" src='{{asset('uploads/images/commons/cards.png')}}' alt='Payment methods'/></div></td></tr>
        <tr>
            <td>{{Form::label('credit_card_number','Credit card number:')}}</td>
            <td style='text-align: right'>{{Form::text('credit_card_number','',array('required'=>'required','placeholder'=>'Your credit card number','class'=>'input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('exp_month','Expiration date:')}}</td>
            <td style='text-align: right'>{{Form::text('exp_month','',array('required'=>'required','placeholder'=>'Month','class'=>'month-input input-text'))}}/
                {{Form::text('exp_year','',array('required'=>'required','placeholder'=>'Year','class'=>'year-input input-text'))}}
                &nbsp;&nbsp;
                {{Form::label('cvc','CVC:')}}&nbsp;
                {{Form::text('card_security_number','',array('required'=>'required','class'=>'cvc-input input-text'))}}</td>
        </tr>
        <tr>
            <td>{{Form::label('area_code','Phone number:')}}</td>
            <td style='text-align: right'>{{Form::text('area_code','',array('required'=>'required','placeholder'=>'Area code','class'=>'phone-area-input input-text'))}}
                {{Form::text('phone_number','',array('required'=>'required','placeholder'=>'Your phone number','class'=>'phone-number-input input-text'))}}
            </td>
        </tr>
        <tr><td></td><td><div class="QapTcha"></div></td></tr>
    </table>
    <br>
    {{Form::hidden('orders_id',$cart->getOrderId())}}
    {{Form::submit('CHECKOUT',array('class'=>'center btn btn-checkout'))}}
    <br>
    {{ Form::close() }}
</div>

<script src='{{asset('js/qaptcha/jquery.ui.touch.js')}}'></script>
<script src='{{asset('js/qaptcha/QapTcha.jquery.js')}}'></script>
<script src='{{asset('js/html2canvas/html2canvas.js')}}'></script>
<script src='{{asset('js/jsPDF/jspdf.js')}}'></script>
<script type='text/javascript'>
    
    $(document).ready(function () {
        $('.QapTcha').QapTcha({
            autoSubmit : true,
            autoRevert : true,
            PHPfile : './qaptcha'
        });
        
        
        $('#name').rules("add", {required: true});
        $('#email').rules("add", {required: true, email: true});
        $(".payment-form").validate({
            errorLabelContainer: $(".payment-form div.error"),
            messages: {
                name: {
                    required: 'Travel date is required!<br>'
                },
                email: {
                    required: 'First Name fields are required!<br>'
                }
            }

        });
        
    });
    $('.btn-download-invoice').click(function() {  
        $("#invoice-container").css('background-color','#fff');
        html2canvas($("#invoice-container"), {
                onrendered: function(canvas) {
                    var imgData = canvas.toDataURL(
                        'image/png');              
                    var doc = new jsPDF("1", "pt", "a4");
                    doc.addImage(imgData, 'PNG', 15, 30,canvas.width/1.6,canvas.height/1.6);
                    doc.save('my-thai-trip-invoice.pdf');
                    $("#invoice-container").css('background-color','#f2f2f2');
                }
        });
       
    });
</script>
@stop