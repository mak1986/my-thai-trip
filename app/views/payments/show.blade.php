@extends('layouts.page')
@section('content')
<h1>Thank you</h1>
<hr>
<div id='receipt-container' style="padding:20px;background-color:#F2F2F2;font-family: 'Open Sans', sans-serif;font-size:13px;line-height: 18px;">
    <table class='receipt-table' witdh="100%">
        <tr>
            <td colspan="4"><span class="logo">My Thai Trip</span></td>
            <td colspan="4" style="text-align: right"><span style="font-size:30px;">RECEIPT OF PAYMENT</span></td>
        </tr>
        <tr>
            <td class='info' width="25%" colspan="2">106/57 M.7 T.Nonghoi A.Muang Chiang Mai 50000 Thailand<br><br></td>
            <td width="13%"></td>
            <td width="12%"></td>   
            <td width="13%"></td>
            <td width="12%"></td>
            <td width="13%"></td> 
            <td width="12%"></td>  
        </tr>
        <tr>
            <td>Tel.:</td>
            <td>087-5805292</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Date:</td>
            <td>{{(new DateTime())->format('d M Y')}}</td>

        </tr>
        <tr>
            <td>Fax:<br></td>
            <td>087-5805215</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Invoice No:</td>
            <td>{{sprintf('%05d', $cart->getOrderId())}}</td>
        </tr>
        <tr>
            <td><br></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Receipt No:</td>
            <td>{{sprintf('%05d', $payment->id)}}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Travel Date:</td>
            <td>{{$cart->getFormatedTravelDate()}}</td>
        </tr>
        <?php 
        $adr = $payment->billing_address_line_1;
        if($payment->billing_address_line_2!='') 
            $adr = "<br>".$payment->billing_address_line_2;
        ?>
        <tr>
            <td>To:</td><td colspan="4">{{$payment->name}}<br>{{$adr}}<br>{{$payment->city}} {{$payment->posttal_code}} {{$payment->country}}</td>
            <td></td><td></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{$payment->email}}</td>
            <td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td>Tel.:</td>
            <td>{{$payment->area_code}} {{$payment->phone_number}}</td>
            <td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
    </table>
    <br>
    @if(!$cart->isEmpty())
    <table class='invoice-table' width="100%">
        <tr>
            <th style="text-align: left">Product Code</th>
            <th style="text-align: left">Product</th>
            <th style="text-align: left">Quantity</th>
            <th style="text-align: right">Unit Price</th>
            <th style="text-align: right">Row Total</th>
        </tr>
        <?php $count = 0; ?>
        @foreach($cart->getItems() as $item)
        <tr>
            <td style="text-align: left">{{$item->getCode()}}</td>
            <td style="text-align: left">{{$item->getName()}}</td>
            <td style="text-align: left">
                <?php
                $countAdults = $item->countAdults();
                $countChildren = $item->countChildren();
                ?>
                @if($countAdults > 0)
                {{$countAdults}} x Adult(s)
                @endif
                @if($countChildren > 0)
                <br>{{$countChildren}} x Child(ren)
                @endif
            </td>
            <?php
            $str = "";
            if ($countAdults > 0)
                $str .= "$ {$item->getPrice()}<br>";
            if ($countChildren > 0)
                $str .= "$ {$item->getChildPrice()}";
            ?>
            <td style="text-align: right">{{$str}}</td>
            <?php
            $str = "";
            if ($countAdults > 0)
                $str .= "$ {$item->getTotalPriceAdult()}<br>";
            if ($countChildren > 0)
                $str .= "$ {$item->getTotalPriceChild()}";
            ?>
            <td style="text-align: right">{{$str}}</td>
        </tr>
        <?php $count++; ?>
        @endforeach

        @if($count<8)
        <?php
        for ($i = 0; ($i + $count) < 8; $i++) {
            ?>
            <tr><td><br></td><td></td><td></td><td></td><td></td></tr>
        <?php } ?>
        @endif
        <tr><td class="invoice-summary"></td><td class="invoice-summary"></td><td class="invoice-summary"></td><td class="invoice-summary" style="text-align: right"><br>Subtotal:</td><td class="invoice-summary" style="text-align:right"><br>$ {{$cart->getTotalPrice()}}</td></tr>
        <tr><td></td><td></td><td></td><td style="text-align: right">VAT Rate:</td><td style="text-align:right">7%</td></tr>
        <tr><td></td><td></td><td></td><td style="text-align: right">VAT:</td><td style="text-align:right">$ {{$cart->calculateVat($cart->getTotalPrice())}}</td></tr>
        <tr><td></td><td></td><td></td><td style="font-weight:bold;text-align: right;">Total:</td><td style="text-align:right;font-weight: bold">$ {{$cart->getTotalPrice()+$cart->calculateVat($cart->getTotalPrice())}}</td></tr>
    </table>

    @else
    <p>No items in your cart...</p>
    @endif
</div>
<div class='center'>
    <button class='center btn btn-download-receipt'>Download</button>
</div>
<script src='{{asset('js/html2canvas/html2canvas.js')}}'></script>
<script src='{{asset('js/jsPDF/jspdf.js')}}'></script>
<script type='text/javascript'>
$('.btn-download-receipt').click(function() {  
        $("#receipt-container").css('background-color','#fff');
        html2canvas($("#receipt-container"), {
                onrendered: function(canvas) {
                    var imgData = canvas.toDataURL(
                        'image/png');              
                    var doc = new jsPDF("1", "pt", "a4");
                    doc.addImage(imgData, 'PNG', 15, 30,canvas.width/1.6,canvas.height/1.6);
                    doc.save('my-thai-trip-receipt.pdf');
                    $("#receipt-container").css('background-color','#f2f2f2');
                }
        });
       
    });
</script>
@stop