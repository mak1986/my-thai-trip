@extends('layouts.master')
@section('h1')
Create promotion
@stop

@section('content')
{{link_to_route('promotions.relations', "Back", $parameters = array($destinations_id), $attributes = array('class'=>'btn btn-default'))}}
<hr>

{{ Form::open(array('route' => 'promotions.store','method'=>'post','role'=>'form','class'=>'row form-horizontal')) }}
@include('includes.error_notification', array('field'=>'discount'))


<div class="form-group">
    {{Form::label('discount', 'Discount',array('class'=>'col-sm-2 control-label'))}}
    <div class="col-sm-2">
        <div class='input-group'>
            {{Form::number('discount','',array('class'=>'input-sm form-control','min'=>"1", 'max'=>"50"))}}
            <div class='input-group-addon'>%</div>
        </div>
    </div>
</div>
@include('includes.error_notification', array('field'=>'from_date'))
@include('includes.error_notification', array('field'=>'to_date'))
<div class="form-group">
    {{Form::label('', 'Period',array('class'=>'col-sm-2 control-label'))}}
    <div class=" col-sm-4">
        <div class="input-daterange input-group">
            {{Form::text('from_date','',array('class'=>'input-sm form-control','id'=>'from_date','readonly'=>'true'))}}
            <div class="input-group-addon">to</div>
            {{Form::text('to_date','',array('class'=>'input-sm form-control','id'=>'to_date','readonly'=>'true'))}}
        </div>
    </div>
</div>
{{Form::hidden('destinations_id',$destinations_id);}}
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{Form::submit('Create',array('class'=>'btn btn-primary'))}}
    </div>
</div>
{{ Form::close() }}

@stop