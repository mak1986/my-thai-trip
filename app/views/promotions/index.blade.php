@extends('layouts.master')

@section('h1')
Promotion for {{$destination->name}} ({{$destination->code}})
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        {{link_to_route('promotions.create', "Create a new promotion", $parameters = array('destinations_id'=>$destination->id), $attributes = array('class'=>'btn btn-success'))}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>Period</th><th>Day(s)</th><th>Discount (%)</th><th>Created at</th><th>Options</th></tr>
            <?php $c = 1;?>
            @foreach ($promotions as $promotion)
            <tr class="@if($c==1) success @endif">
                <td width="35">{{$promotion->from_date}} - {{$promotion->to_date}}</td>
                <td width="20%">{{(new DateTime($promotion->from_date))->diff(new DateTime($promotion->to_date))->format('%a')}}</td>
                <td width="25%">{{$promotion->discount}}</td>
                <td width="15%">{{$promotion->created_at}}</td>
                <td width="5%">
                    {{Form::open(array('route' => array('promotions.destroy', $promotion->id),'method'=>'delete','style'=>'display:inline'))}}
                    {{Form::submit('Delete',array('class'=>'btn btn-danger','onClick'=>'return (confirm("Are you sure?"))'))}}
                    {{Form::close()}}
                </td>
            </tr>
            <?php $c++; ?>
            @endforeach
        </table>

    </div>
</div>
@stop





