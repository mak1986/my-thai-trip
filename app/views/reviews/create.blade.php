<div class="review-form-container">
@include('includes.success_notification')
@include('includes.error_notification',array('field'=>'review'))
@include('includes.error_notification',array('field'=>'review_email'))
{{ Form::open(array('route' => 'reviews.store','class'=>'review-form','method'=>'post','role'=>'form')) }}
<strong>{{Form::label('review','Write a review:')}}</strong><br>
{{Form::textarea('review','',array('cols'=>'108','required'=>'required','class'=>'input-textearea form-control'))}}
<strong>{{Form::label('review_email','Email:')}}</strong> <br>
    <p style="font-size:10px">Note: Use the email corresponding to your previous order to post a review.</p>
{{Form::text('review_email','',array('id'=>'review_email','style'=>'width:400px;','required'=>'required','class'=>'input-text form-control'))}}
{{Form::hidden('destinations_id',$destination_id)}}<br>
{{Form::submit('SUBMIT',array('class'=>'btn btn-send-contact'))}}
{{ Form::close() }}
</div>