@extends('layouts.master')

@section('h1')
Reviews
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>#</th><th>Destination</th><th>Review</th><th>Created at</th><th>Status</th><th>Options</th></tr>
            <?php $c = 1; ?>
            @foreach ($reviews as $review)
            <tr {{($review->status)?"class='success'":""}}>
                <td width="3%">{{$c}}</td>
                <td width="12%">{{$review->destination->name}}</td>
                <td width="44%">{{$review->review}}</td>
                <td width="12">
                <?php $datetime = new DateTime($review->created_at); ?>
                    {{$datetime->format('d M Y H:i')}}</td>
                <td width="8%">{{($review->status)?"Enable":"Disable"}}</td>
                <td width="21%">
                    {{link_to_route('reviews.setStatus', "Enable", $parameters = array($review->id,1), $attributes = array('class'=>'btn btn-success'))}}
                    {{link_to_route('reviews.setStatus', "Disable", $parameters = array($review->id,0), $attributes = array('class'=>'btn btn-warning'))}}
                    {{link_to_route('reviews.delete', "Delete", $parameters = array($review->id), $attributes = array('class'=>'btn btn-danger'))}}

                </td>
            </tr>
            <?php $c++; ?>
            @endforeach
        </table>

    </div>
</div>
@stop





