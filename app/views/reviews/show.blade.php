
    <h4>REVIEWS</h4>
    <hr>
    @if(sizeof($reviews)>0)
        @foreach($reviews as $review)
        <div class="review">
            @foreach($review->payments as $payment)
                <?php $names = explode(" ",$payment->name); ?>
                    <p class="review-metadata">{{$names[0]}} 
                    @for($i=1;$i<sizeof($names);$i++)
                        {{substr($names[$i],0,1)."."}}
                    @endfor
                    </p>
                    <p class="review-metadata">from {{$payment->country}}</p>
                <?php break; ?>
            @endforeach
            <?php $date = new DateTime($review->created_at);?>

            <p class="review-metadata">posted at {{$date->format('d M Y H:i')}}</p>

            <p><img src="{{asset('uploads/images/commons/Quote-icon.png')}}" width="15"/> &nbsp;{{$review->review}}</p>
        </div>
        @endforeach
    @else
    <p>No reviews has been made yet.</p>
    @endif