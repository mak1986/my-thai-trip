@extends('layouts.master')

@section('h1')
Travellers
@stop

@section('content')


<div class="row">
    <div class="col-lg-12">
        @include('includes.success_notification')
        <table class="table table-striped">
            <tr><th>#</th><th>Date</th><th style='text-align: right;'>Orders</th><th style='text-align: right;'>Travellers</th><th style='text-align: center;'>Options</th></tr>
            <?php $c = 1; ?>
            @foreach ($records as $record)
            <tr <?php echo ((new DateTime())<(new DateTime($record['date'])))?"class='info'":"";?>>
                <td width='20%'>{{$c}}<?php $c++; ?></td>
                <td width="20%">{{$record['formatted_date']}}</td>
                <td width="20%" style='text-align: right;'>{{$record['orders']}}</td>
                <td width='20%' style='text-align: right;'>{{$record['travellers']}}</td>

                <td width="20%" style='text-align: center;' >
                    {{link_to_route('travellers.show', "Show", $parameters = array($record['date']), $attributes = array('class'=>'btn btn-primary'))}}
                </td>
            </tr>
            @endforeach
        </table>

    </div>
</div>
@stop





