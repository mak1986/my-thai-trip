@extends('layouts.master')

@section('h1')
Travellers at {{$date}}
@stop

@section('content')
{{link_to_route('travellers.index', "Back", $parameters = array(), $attributes = array('class'=>'btn btn-default'))}}
<button class='btn btn-default' onClick='javascript:download("{{$date}}")'>Download</button>
<hr>

<div class="row">
    <div class="col-lg-12">


        <div id='content'>
        <?php
        $c = 1;
        ?>
            <p><strong>Date of travel:</strong> {{$date}}</p>
        @foreach ($orders as $order)
        <p><strong>Order no.:</strong> {{$order->id}}<br> <strong>Receipt no.:</strong> {{$order->payment->id}}<br> <strong>Contact person:</strong> {{$order->payment->name}}<br> <strong>Phone number:</strong> {{$order->payment->phone_number}}</p>

        <table class="table table-striped">
            <tr><th>#</th><th>First Name</th><th>Last Name</th><th style='text-align: right'>Age</th><th></th></tr>
            @foreach($order->travellers as $traveller)
            <tr>
                <td width='5%'>{{$c}}<?php $c++; ?></td>
                <td width="15%">{{$traveller->first_name}}</td>
                <td width="15%">{{$traveller->last_name}}</td>
                <td width="10%" style='text-align: right'>{{$traveller->age}}</td>
                <td width="55%"></td>
            </tr>
            @endforeach
            <?php $c = 1; ?>
        </table>
        @endforeach

        </div>
    </div>
</div>
<script src='{{asset('js/html2canvas/html2canvas.js')}}'></script>
<script src='{{asset('js/jsPDF/jspdf.js')}}'></script>
<script type='text/javascript'>
            function download(filename){
                    html2canvas($("#content"), {
                    onrendered: function(canvas) {
                    var imgData = canvas.toDataURL(
                            'image/png');
                            var doc = new jsPDF("1", "pt", "a4");
                            doc.addImage(imgData, 'PNG', 15, 30, canvas.width / 1.7, canvas.height / 1.7);
                            doc.save('traveller_'+filename+'.pdf');
                    }
                });
            }
</script>
@stop





